<?php

/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 23/11/2018
 * Time: 08:13
 */
class ProductionManage extends CI_Controller
{

    public $data=array('subview'=>'Oops subview not set','currentdata'=>'','RequestOrder'=>'','rawlist'=>'');

    //Set current user data in topheader
    public function  GetCurrentUserData()
    {
        $this->load->model('Model_user');
        $result=$this->Model_user->GetUserData();
        $this->data['currentdata']=$result;

    }

    //--------------------------------------------------------Production Request Manage section-------------------------------------------------


    //Load Products manage ui
    public function ProductionRequest()
    {

        $this->GetCurrentUserData();
        $this->data['subview']='Production/Request';
        $this->load->view('Home',$this->data);
    }

    //Load Products manage ui
    public function ProductionProcessing()
    {

        $this->GetCurrentUserData();
        $this->data['subview']='Production/Process';
        $this->load->view('Home',$this->data);
    }

    public function ProductionDone()
    {

        $this->GetCurrentUserData();
        $this->data['subview']='Production/Done';
        $this->load->view('Home',$this->data);
    }


    //Load Send process manage ui
    public function SendToProcess()
    {

        $id = $this->input->get('id');
        $this->load->model('Model_rawstock');
        $this->load->model('Model_production');
        $dataset=$this->Model_production->GetProductionRequestData($id);

        //getting raw list

        $raw=$this->Model_rawstock->FetchRawMaterialsForDropdown();
        $this->data['rawlist']= $raw;

        $this->GetCurrentUserData();
        $this->data['RequestOrder']=$dataset;
        $this->data['subview']='Production/SendToProcess';
        $this->load->view('Home',$this->data);
    }

    public function SendToDone()
    {
        $id = $this->input->get('id');
        $this->load->model('Model_production');
        $result=$this->Model_production->SetTodone($id);
        if($result==1){
            $this->session->set_flashdata('msgS','Successfully set to Done !!');
            redirect('ProductionManage/ProductionProcessing');
        }else{
            $this->session->set_flashdata('msgW','oops Something went wrong !!');
            redirect('ProductionManage/ProductionProcessing');
        }


    }

    //Load production request table
    Public function LoadProductionRequestDataTable()
    {
        $query = '';
        $this->load->model('Model_production');
        $this->load->library("pagination");
        $config = array();
        $config["base_url"] = "#";
        $config["total_rows"] = $this->Model_production->count_all();
        $config["per_page"] = 4;
        $config["uri_segment"] = 3;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li>';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li>';
        $config["last_tag_close"] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>Previous Page';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li>";
        $config["num_tag_close"] = "</li>";
        $config["num_links"] = 1;

        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $start = ($page - 1) * $config["per_page"];
        if ($this->input->get('querydata'))
        {
            $query = $this->input->get('querydata');
        }

        $output=array(
            'paginationdata' => $this->pagination->create_links(),
            'ProductionRequesttable'  => $this->Model_production->FetchingProductionRequestDataForTable($config["per_page"],$start, $query)
        );

        echo json_encode($output);
    }

    public function LoadProductionProcessDataTable()
    {
        $query = '';
        $this->load->model('Model_production');
        $this->load->library("pagination");
        $config = array();
        $config["base_url"] = "#";
        $config["total_rows"] = $this->Model_production->countprocess_all();
        $config["per_page"] = 4;
        $config["uri_segment"] = 3;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li>';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li>';
        $config["last_tag_close"] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>Previous Page';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li>";
        $config["num_tag_close"] = "</li>";
        $config["num_links"] = 1;

        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $start = ($page - 1) * $config["per_page"];
        if ($this->input->get('querydata'))
        {
            $query = $this->input->get('querydata');
        }

        $output=array(
            'paginationdata' => $this->pagination->create_links(),
            'ProductionProcesstable'  => $this->Model_production->FetchingProductionProcessDataForTable($config["per_page"],$start, $query)
        );

        echo json_encode($output);

    }

    public function LoadProductiondoneDataTable()
    {
        $query = '';
        $this->load->model('Model_production');
        $this->load->library("pagination");
        $config = array();
        $config["base_url"] = "#";
        $config["total_rows"] = $this->Model_production->countdone_all();
        $config["per_page"] = 4;
        $config["uri_segment"] = 3;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li>';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li>';
        $config["last_tag_close"] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>Previous Page';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li>";
        $config["num_tag_close"] = "</li>";
        $config["num_links"] = 1;

        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $start = ($page - 1) * $config["per_page"];
        if ($this->input->get('querydata'))
        {
            $query = $this->input->get('querydata');
        }

        $output=array(
            'paginationdata' => $this->pagination->create_links(),
            'Productiondonetable'  => $this->Model_production->FetchingProductionDoneDataForTable($config["per_page"],$start, $query)
        );

        echo json_encode($output);
    }

    //add raw material usage for item production
    public function RawMaterialUsageForOrders()
    {
        $rawmaterials = $this->input->get('RawMaterials');
        $requireamount = $this->input->get('RequireAmount');
        $oid = $this->input->get('OID');

        if(count(array_unique($rawmaterials))<count($rawmaterials))
        {
            $output=array(
                'msg' => 'sameraw'
            );

            echo json_encode($output);
        }
        else
        {

            $this->load->model('Model_production');
            $result=$this->Model_production->InsertRawMatUsageForOrder($rawmaterials,$requireamount,$oid);
            if($result==1){
                $output=array(
                    'msg' => 'notenough'
                );
            }else{
                $output=array(
                    'msg' => 'success'
                );
            }
            echo json_encode($output);
        }


    }


}