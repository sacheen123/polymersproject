

<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">User Managment</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Create User</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<div  class="row">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-body">
                <h3 style="padding-bottom: 1%;" class="card-title">Add New User</h3>
                <?php $data = array('onsubmit' => 'return validateRequire()','name'=>'UserAddForm'); ?>
                <?php  echo form_open('UsersManage/InsertNewUser',$data); ?>
                <div class="row">
                    <div class="col-md-6">
                        <div  class="form-group row">

                            <div class="col-10">
                                <label style="color: white" for="example-text-input" class=" col-form-label">User Name</label>
                                <input   autocomplete="off" placeholder="Ex: Jhon" style="color: white" class="form-control" type="text" value="" name="uname">
                            </div>
                            <div id="usernamecontent"></div>
                            <?php echo form_error('uname', '<div style="margin-left: 3%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                        <div class="form-group row">

                            <div class="col-10">
                                <label style="color: white" for="example-search-input" class=" col-form-label">First Name</label>
                                <input autocomplete="off" placeholder="Ex: Sam" style="color: white"  class="form-control" type="text" value="" name="fname">
                            </div>
                            <div id="fnamecontent"></div>
                            <?php echo form_error('fname', '<div style="margin-left: 3%; margin-top: 0.3%;" class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                        <div class="form-group row">

                            <div class="col-10">
                                <label style="color: white" for="example-email-input" class=" col-form-label">Last Name</label>
                                <input placeholder="Ex: Jhonny" style="color: white"  class="form-control" type="text" value="" name="lname">
                            </div>
                            <div id="lnamecontent"></div>
                            <?php echo form_error('lname', '<div style="margin-left: 3%; margin-top: 0.3%;" class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                        <div class="form-group row">

                            <div class="col-10">
                                <label style="color: white" for="example-email-input" class="col-form-label">Phone Number</label>
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">+94</span>
                                    <input placeholder="Ex: 112859319" style="color: white"  class="form-control" type="text" value="" name="pnumber">
                                </div>

                            </div>
                            <div id="phonecontent"></div>
                            <?php echo form_error('pnumber', '<div style="margin-left: 3%;margin-top: 0.3%;" class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">

                            <div class="col-10">
                                <label style="color: white" for="example-email-input" class=" col-form-label">Password</label>
                                <input id="password" placeholder="Ex: 123" style="color: white"  class="form-control" type="password" value="" name="password">
                            </div>
                            <div id="passwordcontent"></div>
                            <?php echo form_error('password', '<div style="margin-left: 3%;margin-top: 0.3%;" class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                        <div class="form-group row">

                            <div class="col-10">
                                <label style="color: white" for="example-email-input" class=" col-form-label">Confirm Password</label>

                                <input onchange='CheckInputPassword()' id="cpassword" placeholder="Ex: 123" style="color: white"  class="form-control" type="password" value="" name="cpassword">
                            </div>
                            <div id="content"></div>
                            <div id="confirmpasswordcontent"></div>
                            <?php echo form_error('cpassword', '<div style="margin-left: 3%;margin-top: 0.3%;" class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                        <div class="form-group row">

                            <div class="col-10">
                                <label style="color: white" for="example-email-input" class=" col-form-label">Email</label>
                                <input onchange="ValidateEmail(document.getElementById('emaildata').value)" id="emaildata" placeholder="Ex: someone@example.com" style="color: white"  class="form-control" type="text" value="" name="email">
                            </div>
                            <div id="emailcontent"></div>
                            <?php echo form_error('email', '<div style="margin-left: 3%;margin-top: 0.3%;" class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                        <div class="form-group row">

                            <div class="col-10">
                                <label style="color: white"  for="example-email-input" class="col-form-label">User Role</label>
                                <select style="color: white;" name="role" class="form-control" id="sel1">
                                    <option value="1">Admin</option>
                                    <option value="2">Stock Manager</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div >
                    <button id="myBtn"  style="padding-left: 3%;padding-right: 3%;" type="submit" class="btn btn-success">Insert</button>
                    <div id="wrongmsg"></div>
                </div>
                    <?php form_close(); ?>
            </div>
        </div>
    </div>

