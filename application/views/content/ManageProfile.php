


<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Manage User Profile</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Manage Profile</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<div class="row">
    <div class="col-lg-8 col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Reset Profile Data</h4>
                <?php echo form_open('UserProfileManage/UpdateUserProfile'); ?>
                <?php   foreach ($currentdata as $udata){ ?>
                    <div class="form-group m-t-40 row">
                        <label for="example-text-input" class="col-2 col-form-label">User Name</label>
                        <div class="col-10">
                            <input style="color: white" class="form-control" type="text" value="<?php echo $udata->username; ?>" name="uname">
                        </div>
                        <?php echo form_error('uname', '<div style="margin-left: 19%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                    </div>
                    <div class="form-group row">
                        <label for="example-search-input" class="col-2 col-form-label">First Name</label>
                        <div class="col-10">
                            <input style="color: white"  class="form-control" type="text" value="<?php echo $udata->fname; ?>" name="fname">
                        </div>
                        <?php echo form_error('fname', '<div style="margin-left: 19%;" class=" tst4 btn btn-danger">', '</div>'); ?>
                    </div>
                    <div class="form-group row">
                        <label for="example-email-input" class="col-2 col-form-label">Last Name</label>
                        <div class="col-10">
                            <input style="color: white"  class="form-control" type="text" value="<?php echo $udata->lname; ?>" name="lname">
                        </div>
                        <?php echo form_error('lname', '<div style="margin-left: 19%;" class=" tst4 btn btn-danger">', '</div>'); ?>
                    </div>
                    <div class="form-group row">
                        <label for="example-email-input" class="col-2 col-form-label">Phone Number</label>
                        <div class="col-10">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1">+94</span>
                                </div>
                                <input style="color: white"  class="form-control" type="text" value="<?php echo $udata->PhoneNumber; ?>" name="pnumber">
                            </div>
                        </div>
                        <?php echo form_error('pnumber', '<div style="margin-left: 19%;margin-top: -3%;" class=" tst4 btn btn-danger">', '</div>'); ?>
                    </div>
                    <div style="padding-left: 18%;">
                    <button type="submit" class="btn btn-info">Update</button>
                    </div>

                <?php echo form_close();?>
            </div>
        </div>
    </div>

    <div class="col-lg-4 col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="d-flex flex-row">
                    <div class=""><img src="http://localhost/App//assets/images/users/5.jpg" alt="user" class="img-circle" width="100"></div>
                    <div class="p-l-20">
                        <h3 class="font-medium"><?php echo $udata->fname ." ".$udata->lname ?></h3>
                        <h6><?php           $this->load->model('Model_user');
                                            $result= $this->Model_user->LoginUserData();
                                            $types=$this->Model_user->DataRetrive('userroles','RoleID',$udata->type,'Role');
                                            echo $types;  ?></h6>
                    </div>
                </div>
            </div>
                <hr>
            <div style="padding: 2%;margin-top: -16px;" class="card">
                <?php echo form_open('UserProfileManage/UpdateUserPassword'); ?>
                <div class="card-body">
                    <small class="text-muted">current Password </small>
                    <div class="col-xs-12">
                        <h6><input style="color: white"  name="cpwd"  style="color: white;" class="form-control" type="password" value="" ></h6>
                    </div>
                    <?php echo form_error('cpwd', '<div class=" tst4 btn btn-danger">', '</div>'); ?>

                    <small class="text-muted">New Password </small>
                    <div class="col-xs-12">
                        <h6><input style="color: white"  name="npwd"  style="color: white;" class="form-control" type="password" value="" ></h6>
                    </div>
                    <?php echo form_error('npwd', '<div class=" tst4 btn btn-danger">', '</div>'); ?>

                </div>
                <div style="padding-left: 8%;">
                    <button type="submit" class="btn btn-success">Reset Password</button>
                </div>
                <?php form_close();?>
            </div>
        </div>
        </div>
    </div>
</div>
<?php } ?>