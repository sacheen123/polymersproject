<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">User Managment</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">View Users</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10"><i
                    class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="card">
    <div style="color: white" class="card-body">
        <h4 class="card-title">Manage Users</h4>
        <div class="table-responsive m-t-40">
            <div id="example23_wrapper" class="dataTables_wrapper">
                <div class="dt-buttons">
                    <form method="get" action="<?php base_url(); ?>ExportAsExcel">
                        <input style="border-color: transparent; cursor: pointer;" value="Excel" type="submit"
                               name="export" class="dt-button buttons-excel buttons-html5" tabindex="0"
                               aria-controls="example23">
                    </form>
                </div>
                <div id="example23_filter" class="dataTables_filter">
                    <label>Search:<input style="color: white;" id="search_text" type="search" class="" placeholder=""
                                         aria-controls="example23"></label>
                </div>
                <div id="user_table" class="dataTables_wrapper"></div>
                <div align="right" id="pagination_link"></div>
            </div>
        </div>
    </div>
</div>
</div>


<script>

    var pagevalue = 1;

    //Delete user
    function DeleteUser(id) {

        $(document).ready(function () {
            var userid = id;
            swal({
                title: "Are you sure?",
                text: "Do yo want to Delete this User!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    url: "<?php echo base_url();?>index.php/UsersManage/DeleteUser",
                    methode: "POST",
                    dataType: "json",
                    data: {"userID": userid},
                    success: function (data) {
                        swal("Deleted!", "Selected user deleted.", "success");
                        LoadUserTable(pagevalue, '');

                    }, error: function (data) {
                        alert("Oops! something went Wrong");
                    }
                });


            });
        });

    }


    //Get active userid and make user inactive
    function GetactiveUserID(id) {
        $(document).ready(function () {
            var userid = id;
            console.log(pagevalue);
            $.ajax({
                url: "<?php echo base_url();?>index.php/UsersManage/MakeInactiveUser",
                methode: "POST",
                dataType: "json",
                data: {"inactiveID": userid},
                success: function (data) {
                    console.log(data);
                    LoadUserTable(pagevalue, '');

                }, error: function (data) {
                    alert("Oops! something went Wrong");
                }
            });
        });
    }

    //Get active userid and make user inactive
    function GetInactiveUserID(id) {
        $(document).ready(function () {
            var userid = id;
            console.log(pagevalue);
            $.ajax({
                url: "<?php echo base_url();?>index.php/UsersManage/MakeactiveUser",
                methode: "POST",
                dataType: "json",
                data: {"inactiveID": userid},
                success: function (data) {
                    console.log(data);
                    LoadUserTable(pagevalue, '');

                }, error: function (data) {
                    alert("Oops! something went Wrong");
                }
            });
        });
    }

    //load user data table function
    window.LoadUserTable = function (page, searchdata) {
        $.ajax({
            url: "<?php echo base_url();?>index.php/UsersManage/LoadUserDataTable/" + page,
            methode: "POST",
            dataType: "json",
            data: {"querydata": searchdata},
            success: function (data) {
                console.log(data)
                $('#user_table').html(data.usertable);
                $('#pagination_link').html(data.paginationdata);
            }, error: function (data) {
                alert("Oops! something went Wrong");
            }
        });
    }


    $(document).ready(function () {
        //calling load data to table function
        LoadUserTable(pagevalue, '');

        //pagination click events
        $(document).on("click", ".pagination li a", function (event) {
            event.preventDefault();
            pagevalue = $(this).data("ci-pagination-page");
            var search = document.getElementById("search_text").value;
            console.log(search);
            LoadUserTable(pagevalue, search);
        });

        //search input click events
        $('#search_text').keyup(function () {
            var search = $(this).val();
            if (search != '') {
                LoadUserTable(pagevalue, search);
            }
            else {
                LoadUserTable(pagevalue, '');
            }
        });


    });
</script>