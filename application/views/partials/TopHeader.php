
<!-- ============================================================== -->
<!-- Displaying flash messages-->
<!-- ============================================================== -->
<!--Displaying Warn message-->
<?php if($this->session->flashdata('msgW')): ?>
    <script>
        swal({
            title: "Oops!!",
            type: "warning",
            text: "<?php echo $this->session->flashdata('msgW') ?>",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "close",
            closeOnConfirm: true

        });
    </script>
<?php endif; ?>
<!--Displaying Success message-->
<?php if($this->session->flashdata('msgS')): ?>
    <script>
        swal({
            title: "Success!!",
            type: "success",
            text: "<?php echo $this->session->flashdata('msgS') ?>",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "close",
            closeOnConfirm: true

        });
    </script>
<?php endif; ?>


<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Topbar header - style you can find in pages.scss -->
<!-- ============================================================== -->
<?php

foreach ($currentdata as $data){ ?>
<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="navbar-header">
            <a class="navbar-brand" href="http://localhost/App">

                <b>
                    <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                    <!-- Dark Logo icon -->
                    <img src="<?php echo base_url()?>/assets/images/logo2.png" alt="homepage" >
                    <!-- Light Logo icon -->

                </b>
                <!--End Logo icon -->
                <!-- Logo text -->


        </div>
        <!-- ============================================================== -->
        <!-- End Logo -->
        <!-- ============================================================== -->
        <div class="navbar-collapse">
            <!-- ============================================================== -->
            <!-- toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav mr-auto mt-md-0 ">
                <!-- This is  -->
                <li class="nav-item"> <a class="nav-link nav-toggler hidden-md-up text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="ti-menu"></i></a> </li>
                <li class="nav-item"> <a class="nav-link sidebartoggler hidden-sm-down text-muted waves-effect waves-dark" href="javascript:void(0)"><i class="icon-arrow-left-circle"></i></a> </li>
                <li><h1 style="margin-top: 2%;font-size: 30px;">Wayamba Polymers (Pvt) Ltd</h1></li>



            </ul>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->

            <ul class="navbar-nav my-lg-0">
                <li class="nav-item dropdown">
                    <a  class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" id="noty" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-bell fa-lg"></i>
                        <div style="    margin-top: 5px;" class="notify">
                            <span class="heartbit"></span>
                            <span  style="width: auto !important;color:red;margin-top: 20px;position: absolute;top: -20px;right: -4px;height: 25px;width: 25px;" >
                                <strong><h5  style="color: #ffeb3b;margin-left: 5px;" ><div id="unseennotifications" style=" padding-right: 5px; padding-left:5px;background-color: red;color: white;" ></div></h5></strong>
                            </span>
                            <span class="point"></span>
                        </div>
                    </a>
                    <div style="    margin-left: -505%; " class="dropdown-menu mailbox animated bounceInDown" aria-labelledby="2">
                        <ul>
                            <li>
                                <div id="testdata" class="drop-title">Notification List</div>
                            </li>
                            <li>
                                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: auto; height: 250px;">
                                        <div class="message-center" style="overflow: hidden; width: auto; height: 250px;">
                                        <!--Messages section-->
                                            <div id="listNotification"></div>
                                        </div>
                                    <div class="slimScrollBar" style="background: rgb(220, 220, 220); width: 5px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div><div class="slimScrollRail" style="width: 5px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div></div>
                            </li>
<!--                            <li>-->
<!--                                <a class="nav-link text-center" href="javascript:void(0);"> <strong>See all e-Mails</strong> <i class="fa fa-angle-right"></i> </a>-->
<!--                            </li>-->
                        </ul>
                    </div>
                </li>



                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo base_url()?>/assets/images/users/5.jpg" alt="user" class="profile-pic" /></a>
                    <div class="dropdown-menu dropdown-menu-right animated flipInY">
                        <ul class="dropdown-user">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="<?php echo base_url()?>assets/images/users/5.jpg" alt="user"></div>
                                    <div class="u-text">
                                        <h4><?php echo $data->fname." ".$data->lname; ?></h4>
                                        <p class="text-muted"><?php echo $data->email; ?></p><button type="button" data-toggle="modal" data-target="#responsive-modal" data-whatever="@mdo" class="btn btn-rounded btn-danger btn-sm">View Profile</button></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo base_url('index.php/UserProfileManage/index')?>"  ><i class="ti-settings"></i> Manage Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="<?php echo base_url('index.php/Login/Logout')?>"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- ============================================================== -->
<!-- End Topbar header -->
<!-- ============================================================== -->
<!-- modal for view profile content -->
<div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Profile Detail</h4>
            </div>
            <div class="modal-body">
                <div >
                    <div class="card">
                        <div class="card-body">
                            <center class="m-t-30"> <img src="<?php echo base_url()?>/assets/images/users/5.jpg" class="img-circle" width="150" />
                                <h4 class="card-title m-t-10"><?php echo $data->fname." ".$data->lname; ?></h4>
                                <h6 class="card-subtitle"><?php  echo $this->session->userdata('type') ?></h6>
                            </center>
                        </div>
                        <div>
                            <hr> </div>
                        <div class="card-body"> <small class="text-muted">Email address </small>
                            <h6><?php echo $data->email; ?></h6> <small class="text-muted p-t-30 db">Phone</small>
                            <h6><?php echo  $data->PhoneNumber; ?></h6> <small class="text-muted p-t-30 db">Registered Date</small>
                            <h6><?php echo  $data->createDate; ?></h6>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<!-- /.view profile modal -->

<script>
    function playSound()
    {
        var audio = new Audio('<?php echo base_url();?>assets/music/tone.MP3');
        audio.play();
    }

    $(document).ready(function () {
        FetchAllNotifications();
        function FetchAllNotifications(view='') {
            $.ajax({
                url:"<?php echo base_url();?>index.php/RawStockManage/FetchAllNotifications" ,
                methode:"POST",
                dataType:"json",
                data:{"view":view},
                success:function (data) {
                    $('#listNotification').html(data.DataList);
                    if (data.UnreadCount>0)
                    {
                        playSound();
                        $('#unseennotifications').html(data.UnreadCount);
                    }
                    console.log(data);
                },error:function () {
                    console.log("oops error!")
                }


            })
        }
        $('.dropdown-toggle').click(function () {
            $('#unseennotifications').html('');
            FetchAllNotifications('yes');
        })
    });

</script>

