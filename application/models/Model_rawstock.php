<?php

/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 06/11/2018
 * Time: 22:58
 */
class Model_rawstock extends CI_Model
{

    //Getting perticular value from entered db table
    function DataRetrive($datatable, $feildvalue, $value, $returnfield)
    {

        $data = "";
        $sql = "SELECT * FROM " . $datatable . " WHERE " . $feildvalue . "=? AND isDeleted='0'";
        $query = $this->db->query($sql, array($value));
        foreach ($query->result() as $row) {
            $data = $row->$returnfield;
        }

        return $data;
    }

    //-----------------------------------------------------Category Section-----------------------------------------------------

    //Insert New category item
    public function InsertNewCategory()
    {
        $cname = strtoupper($this->input->post('cname', TRUE));
        $mtype = $this->input->post('mtype', TRUE);
        $date = date('Y-m-d H:i:s');
        $CategoryNameAvailability = $this->DataRetrive('categories', 'cname', $cname, 'cid');
        if ($CategoryNameAvailability == '') {
            $sql = "INSERT INTO categories (cname,mtype,InsertDate,isDeleted) values (?,?,?,'0') ";
            $result = $this->db->query($sql, array($cname, $mtype, $date));
            if ($result == 1) {
                return 1;
            } else {
                return 3;
            }
        } else {
            return 2; //username already available
        }
    }


    //count all users for pagination function
    function count_all()
    {
        $this->db->select("*");
        $this->db->from("categories");
        $this->db->where("isDeleted", "0");
        $query = $this->db->get();
        return $query->num_rows();
    }

    //fetching categorydata required for category table
    function FetchingCategoryDataForTable($limit, $start, $searchquery)
    {
        $output = '';
        $this->db->select("*");
        $this->db->from("categories");
        $this->db->where("isDeleted", "0");
        if ($searchquery != '') {
            $this->db->like('cname', $searchquery);
        }
        $this->db->order_by("cid");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $output .= '
        <table class="display nowrap table table-hover table-striped table-bordered dataTable"
           cellspacing="0" width="100%" role="grid" aria-describedby="example23_info" style="width: 100%;">
        <thead>
        <tr role="row">
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending"
                aria-label="Name: activate to sort column descending" style="width: 175px;">Category Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Position: activate to sort column ascending" style="width: 254px;">Measurement
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Office: activate to sort column ascending" style="width: 133px;">Action
            </th>
        </tr>
        </thead>
            ';
            foreach ($query->result() as $row) {

                $mtypes = $this->DataRetrive('measurements', 'mid', $row->mtype, 'mesurement');

                $output .= '
           <tr>
             <td>' . $row->cname . '</td>
             <td>' . $mtypes . '</td>
             <td class="text-nowrap">
                <div style=" margin-left: 20%;">
                    <a  href=\'' . base_url() . "index.php/RawStockManage/EditCategory?id=$row->cid" . '\'  data-toggle="tooltip" data-original-title="Edit"> <i  class="fa fa-pencil  m-r-10"></i> </a>
                    <a style="cursor: pointer;" onclick="DeleteCategory(\'' . $row->cid . '\')"  data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>                 
                 </div>
                           
             </td>
           </tr>
           ';
            }
            $output .= '</table>';
        } else {
            $output .= '<tr>
       <td colspan="5">No Data Found</td>
      </tr>
      
      ';
        }
        return $output;
    }

    //delete category
    public function DeleteCategory($id)
    {
        $sql = "UPDATE categories SET  isDeleted='1'  WHERE cid=? ";
        $this->db->query($sql, array($id));
        return 'true';
    }

    //Fetching requested category details to load in Edit category form
    public function GetRequestedStockData($categoryid)
    {
        $sql = "SELECT * FROM  categories WHERE cid=?";
        $result = $this->db->query($sql, array($categoryid));
        return $result->result();
    }

    //update the category details
    public function UpdateCurrentCategory($id)
    {
        $cname = $this->input->post('cname', TRUE);
        $mtype = $this->input->post('mtype', TRUE);

        $CatNameAvailability = $this->DataRetrive('categories', 'cname', $cname, 'cid');
        $currentcatname=$this->DataRetrive('categories', 'cid', $id, 'cname');
        if ($CatNameAvailability!='' && $cname!=$currentcatname){
            return 2;
        }else{
            $sql = "UPDATE categories SET cname=?,mtype=? WHERE cid=? ";
            $result = $this->db->query($sql, array($cname, $mtype, $id));
            return 1;
        }




    }

    //Fetching all the category data for load in dropdown
    public function FetchCategoryForDropdown()
    {
        $this->db->select("*");
        $this->db->from("categories");
        $this->db->where("isDeleted", "0");
        $this->db->order_by("cid");
        $query = $this->db->get();
        return $query->result();

    }

    //-------------------------------------------------------------Raw material section-----------------------------------------------------

    //Inserting new raw item
    public function InsertNewRawItem()
    {

        $rname = $this->input->post('rname', TRUE);
        $camount = $this->input->post('camount', TRUE);
        $unitprice = $this->input->post('unitprice', TRUE);
        $ctype = $this->input->post('categorytype', TRUE);
        $date = date('Y-m-d H:i:s');
        $RawNameAvailability = $this->DataRetrive('rawmaterials', 'Rname', $rname, 'RID');
        if ($RawNameAvailability == '') {
            $sql = "INSERT INTO rawmaterials (Rname,Rcategoryid,oneprice,criticalamount,currentamount,insertdate,isDeleted) values (?,?,?,?,'0',?,'0') ";
            $result = $this->db->query($sql, array($rname, $ctype, $unitprice, $camount, $date));
            if ($result == 1) {
                return 1;
            } else {
                return 3;
            }
        } else {
            return 2; //raw material name already available
        }
    }


    //fetching all the raw materials data load in dropdown
    public function FetchRawMaterialsForDropdown()
    {
        $this->db->select("*");
        $this->db->from("rawmaterials");
        $this->db->where("isDeleted", "0");
        $this->db->order_by("RID");
        $query = $this->db->get();
        return $query->result();

    }

    //updating raw itme available amount
    public function UpdateRawItemavailbleAmount($state)
    {
        $rid = $this->input->post('rawtype', TRUE);
        $amount = $this->input->post('updateamount', TRUE);
        $RawNameAvailability = $this->DataRetrive('rawmaterials', 'RID', $rid, 'Rname');
        if ($RawNameAvailability != '') {

            //get the currentle available amount
            $CurrentavailableAmount = $this->DataRetrive('rawmaterials', 'RID', $rid, 'currentamount');
            if ($state == 'add') {
                $NewAmount = $CurrentavailableAmount + $amount;
            } else {
                if ($CurrentavailableAmount == 0) {
                    return 4;
                } else {
                    $NewAmount = $CurrentavailableAmount - $amount;
                    if ($NewAmount<0)
                    {
                        return 5;
                    }
                }

            }


            $sql = "UPDATE rawmaterials SET currentamount=? WHERE RID=? ";
            $result = $this->db->query($sql, array($NewAmount, $rid));
            if ($result == 1) {
                return 1;
            } else {
                return 3;
            }
        } else {
            return 2; //raw material not available
        }
    }

    //count all raw materials
    public function count_all_Rawmaterials()
    {
        $this->db->select("*");
        $this->db->from("rawmaterials");
        $this->db->where("isDeleted", "0");
        $query = $this->db->get();
        return $query->num_rows();

    }

    //fetching raw details required for rawitems table
    function FetchingRawItemDataForTable($limit, $start, $searchquery)
    {
        $output = '';
        $this->db->select("*");
        $this->db->from("rawmaterials");
        $this->db->where("isDeleted", "0");
        if ($searchquery != '') {
            $this->db->like('Rname', $searchquery);
        }
        $this->db->order_by("RID");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $output .= '
        <table class="display nowrap table table-hover table-striped table-bordered dataTable"
           cellspacing="0" width="100%" role="grid" aria-describedby="example23_info" style="width: 100%;">
        <thead>
        <tr role="row">
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending"
                aria-label="Name: activate to sort column descending" style="width: 175px;">R.Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Position: activate to sort column ascending" style="width: 254px;">Category
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Office: activate to sort column ascending" style="width: 133px;">Unit Price
            </th>
             <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Office: activate to sort column ascending" style="width: 133px;">Critical amount
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Office: activate to sort column ascending" style="width: 133px;">Available amount
            </th>      
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Office: activate to sort column ascending" style="width: 133px;">Action
            </th> 
        </tr>
        </thead>
            ';
            foreach ($query->result() as $row) {

                $categorytypes = $this->DataRetrive('categories', 'cid', $row->Rcategoryid, 'cname');

                $output .= '
           <tr>
             <td>' . $row->Rname . '</td>
             <td>' . $categorytypes . '</td>
             <td>' . $row->oneprice . '</td>
             <td>' . $row->criticalamount . '</td>
             <td>' . $row->currentamount . '</td>
             <td class="text-nowrap">
                <div style=" margin-left: 20%;">
                    <a  href=\'' . base_url() . "index.php/RawStockManage/EditRawMaterial?id=$row->RID" . '\'  data-toggle="tooltip" data-original-title="Edit"> <i  class="fa fa-pencil  m-r-10"></i> </a>
                    <a style="cursor: pointer;" onclick="DeleteRawData(\'' . $row->RID . '\')"  data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>                 
                 </div>
                           
             </td>
           </tr>
           ';
            }
            $output .= '</table>';
        } else {
            $output .= '<tr>
       <td colspan="5">No Data Found</td>
      </tr>
      
      ';
        }
        return $output;
    }

    //delete raw matirial data
    public function DeleteRawMaterialData($id)
    {
        $sql = "UPDATE rawmaterials SET  isDeleted='1'  WHERE RID=? ";
        $this->db->query($sql, array($id));
        return 'true';
    }

    //get request editing rawmatirial data
    public function GetRequestedRawMatirialData($rid)
    {
        $sql = "SELECT * FROM   rawmaterials WHERE RID=?";
        $result = $this->db->query($sql, array($rid));
        return $result->result();
    }

    //update the raw matirials data
    public function UpdateCurrentRawMaterial($id)
    {
        $rname = $this->input->post('rname', TRUE);
        $camount = $this->input->post('camount', TRUE);
        $unitprice = $this->input->post('unitprice', TRUE);
        $ctype = $this->input->post('categorytype', TRUE);
        $RawNameAvailability = $this->DataRetrive('rawmaterials', 'Rname', $rname, 'RID');
        $currentrawname=$this->DataRetrive('rawmaterials', 'RID', $id, 'Rname');
        if ($RawNameAvailability!='' && $rname!=$currentrawname){
            return 2;
        }else{
            $sql = "UPDATE rawmaterials SET Rname=?,criticalamount=?,oneprice=?,Rcategoryid=? WHERE RID=? ";
            $result = $this->db->query($sql, array($rname, $camount, $unitprice, $ctype, $id));
            return 1;
        }


    }

    //------------------------------------------Report section---------------------------------------------
    public function FetchforexcelRawMaterials()
    {
        $sql = "SELECT * FROM rawmaterials WHERE isDeleted='0'";
        $result = $this->db->query($sql);
        return $result->result();
    }

    //------------------------------------------Measurment section--------------------------------------------
    public function InsertNewMeasurement()
    {
        $mesurement = $this->input->post('mesname', TRUE);
        $RawNameAvailability = $this->DataRetrive('measurements', 'mesurement', $mesurement, 'mid');
        if ($RawNameAvailability == '') {
            $sql = "INSERT INTO measurements (mesurement,isDeleted) values (?,'0') ";
            $result = $this->db->query($sql, array($mesurement));
            if ($result == 1) {
                return 1;
            } else {
                return 3;
            }
        } else {
            return 2; //measurment  name already available
        }
    }

    public function RetriveAllMeasuremnts()
    {
        $sql = "SELECT * FROM measurements ";
        $result = $this->db->query($sql);
        return $result->result();
    }

    //-------------------------------------------------Notification section------------------------------------

    //Insert notification data to notification table
    public function CriticalNotificationCheck($id, $changeamount)
    {
        $RawMaterialName = $this->DataRetrive('rawmaterials', 'RID', $id, 'Rname');
        $CurrentavailableAmount = $this->DataRetrive('rawmaterials', 'RID', $id, 'currentamount');
        $CriticalAmount = $this->DataRetrive('rawmaterials', 'RID', $id, 'criticalamount');
        $date = date('Y-m-d H:i:s');

        $CurrentAmountAfterChange = $CurrentavailableAmount - $changeamount;
        if ($CurrentAmountAfterChange == $CriticalAmount || $CurrentAmountAfterChange < $CriticalAmount) {
            $NotificationHeader = "Raw material Out Stock";
            $NotificationBody = $RawMaterialName . " Out Of stock";

            $sql = "INSERT INTO Notification (NotificationHeader,NotificationBody,notificationtype,NotificationDate,NotificationStatus) values (?,?,'raw',?,0) ";
            $result = $this->db->query($sql, array($NotificationHeader, $NotificationBody, $date));
            if ($result == 1) {
                return 1;
            } else {
                return 3;
            }

        } else {

            return 2;
        }

    }


    //retrive all notification data
    public function FetchNotifications()
    {
        $output = '';
        $sql = "SELECT * FROM   notification   ORDER BY NID DESC LIMIT 5";
        $result = $this->db->query($sql);

        if ($result->num_rows() != 0) {
            foreach ($result->result() as $row) {
                if ($row->notificationtype=='raw')
                {
                    $output .= '

                    <a href="#">
                    <div class="row">
                    <div style="padding-top: 7px;" class="col-md-2">
                     <div class="btn btn-success btn-circle"><i class="fa fa-shopping-bag"></i></div>
                    </div>
                    <div class="col-md-10">
                    <div class="mail-contnet">
                        <h5>' . $row->NotificationHeader . '</h5> <span style="color:white;" class="mail-desc">' . $row->NotificationBody . '</span> <span style="color:white;" class="time">' . $row->NotificationDate . '</span> </div>
                    </div>
                    </div>

                    </a>
           ';
                }elseif ($row->notificationtype=='product'){
                    $output .= '

                    <a href="#">
                    <div class="row">
                    <div style="padding-top: 7px;" class="col-md-2">
                     <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                    </div>
                    <div class="col-md-10">
                    <div class="mail-contnet">
                        <h5>' . $row->NotificationHeader . '</h5> <span style="color:white;" class="mail-desc">' . $row->NotificationBody . '</span> <span style="color:white;" class="time">' . $row->NotificationDate . '</span> </div>
                    </div>
                    </div>

                    </a>
           ';
                }elseif ($row->notificationtype=='productionrequest') {
                    $output .= '

                    <a href="#">
                    <div class="row">
                    <div style="padding-top: 7px;" class="col-md-2">
                     <div class="btn btn-primary btn-circle"><i class="fa fa-gear"></i></div>
                    </div>
                    <div class="col-md-10">
                    <div class="mail-contnet">
                        <h5>' . $row->NotificationHeader . '</h5> <span style="color:white;" class="mail-desc">' . $row->NotificationBody . '</span> <span style="color:white;" class="time">' . $row->NotificationDate . '</span> </div>
                    </div>
                    </div>

                    </a>
           ';
                }

            }
        } else {
            $output .= '<tr>
       <td colspan="5">No Data Found</td>
      </tr>
      
      ';
        }

        return $output;
    }

    public function CountUnreadMsg()
    {
        $sql = "SELECT * FROM   notification WHERE NotificationStatus=0";
        $result = $this->db->query($sql);
        $output=$result->num_rows();
        return $output;
    }

    public function SetReadNotifications()
    {
        $sql = "UPDATE notification SET  NotificationStatus='1'  WHERE NotificationStatus='0' ";
        $this->db->query($sql);
    }




}



