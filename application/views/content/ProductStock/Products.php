<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Product Stock Managment</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Products Manage</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10"><i
                    class="ti-settings text-white"></i></button>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div style="color: white;padding: 17px" class="card-body">
                <h4 class="card-title">View Products</h4>
                <div class="table-responsive m-t-40">
                    <div id="example23_wrapper" class="dataTables_wrapper">
                        <div class="dt-buttons">
<!--                            <form method="get" action="--><?php //base_url(); ?><!--ExportAsExcel">-->
<!--                                <input style="border-color: transparent; cursor: pointer;" value="Excel" type="submit"-->
<!--                                       name="export" class="dt-button buttons-excel buttons-html5" tabindex="0"-->
<!--                                       aria-controls="example23"></input>-->
<!--                            </form>-->
                        </div>
                        <div id="example23_filter" class="dataTables_filter">
                            <label>Search:<input placeholder="Raw Material Name" style="color: white;" id="search_text"
                                                 type="search" class="" aria-controls="example23"></label>
                        </div>
                        <div id="ProductMaterial_table" class="dataTables_wrapper"></div>
                        <div align="right" id="pagination_link"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>





  <?php if(!empty($productlist)){  ?>
    <div class="col-md-6">
        <div class="card">
            <div style="color: white" class="card-body">
                <h4 class="card-title">Update Product</h4>

                <?php foreach ($productlist as $d){ ?>
                <?php $dataset = array('onsubmit' => 'return validateRequireproduct()', 'name' => 'productForm'); ?>
                <?php echo form_open('ProductStockManage/UpdateProduct?id='.$d->PID,$dataset); ?>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-10">
                                <label style="color: white" for="example-text-input" class=" col-form-label">Product Item</label>
                                <input style="text-transform: uppercase; color:white;" autocomplete="off" placeholder="Ex: 15L SWING BIN"
                                       style="color: white" class="form-control" type="text" value="<?php echo $d->productname?>" name="pname">
                            </div>
                            <div id="pnamecontent"></div>
                            <?php echo form_error('pname', '<div style="margin-left: -2%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-10">
                                <label style="color: white" for="example-text-input" class=" col-form-label">Unit Price(Rs)</label>
                                <input style=" color:white;" autocomplete="off" placeholder="Ex:200"
                                       style="color: white" class="form-control" type="text" value="<?php echo $d->unitprice ?>" name="unitprice">
                            </div>
                            <div id="unitpricecontent"></div>
                            <?php echo form_error('unitprice', '<div style="margin-left: 14%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-10">
                                <label style="color: white" for="example-text-input" class=" col-form-label">Critical amount</label>
                                <input style="text-transform: uppercase; color:white;" autocomplete="off"
                                       class="form-control" type="text" value="<?php echo $d->criticalamount ?>" name="criamount">
                            </div>
                            <div id="criamountcontent"></div>
                            <?php echo form_error('criamount', '<div style="margin-left: -2%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-10">
                                <label style="color: white" for="example-text-input" class=" col-form-label">Available Amount</label>
                                <input style=" color:white;" autocomplete="off" placeholder="Ex:200"
                                       style="color: white" class="form-control" type="text" value="<?php echo $d->availableamount ?>" name="avaiamount">
                            </div>
                            <div id="avaiamountcontent"></div>
                            <?php echo form_error('avaiamount', '<div style="margin-left: 14%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                    </div>
                </div>
                <div>

                    <button id="myBtna" style="padding-left: 3%;padding-right: 3%;" type="submit"
                            class="btn btn-success">Update
                    </button>
                    <a href="<?php echo base_url()?>index.php/ProductStockManage/Products"  style="padding-left: 3%;padding-right: 3%;" type="submit"
                            class="btn btn-danger">Back
                    </a>
                    <div id="wrongmsg"></div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
 <?php }else{  ?>
    <div class="col-md-6">
        <div class="card">
            <div style="color: white" class="card-body">
                <h4 class="card-title">Insert New Product</h4>
                <?php $dataset = array('onsubmit' => 'return validateRequireproduct()', 'name' => 'productForm'); ?>
                <?php echo form_open('ProductStockManage/InsertProduct',$dataset); ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-10">
                                <label style="color: white" for="example-text-input" class=" col-form-label">Product Item</label>
                                <input style="text-transform: uppercase; color:white;" autocomplete="off" placeholder="Ex: 15L SWING BIN"
                                      class="form-control" type="text" value="" name="pname">
                            </div>
                            <div id="pnamecontent"></div>
                            <?php echo form_error('pname', '<div style="margin-left: -2%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-10">
                                <label style="color: white" for="example-text-input" class=" col-form-label">Unit Price(Rs)</label>
                                <input style=" color:white;" autocomplete="off" placeholder="Ex:200"
                                       style="color: white" class="form-control" type="text" value="" name="unitprice">
                            </div>
                            <div id="unitpricecontent"></div>
                            <?php echo form_error('unitprice', '<div style="margin-left: 14%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-10">
                                <label style="color: white" for="example-text-input" class=" col-form-label">Critical amount</label>
                                <input style="text-transform: uppercase; color:white;" autocomplete="off" placeholder="Ex: 120"
                                       class="form-control" type="text" value="" name="criamount">
                            </div>
                            <div id="criamountcontent"></div>
                            <?php echo form_error('criamount', '<div style="margin-left: -2%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-10">
                                <label style="color: white" for="example-text-input" class=" col-form-label">Available Amount</label>
                                <input style=" color:white;" autocomplete="off" placeholder="Ex:200"
                                       style="color: white" class="form-control" type="text" value="" name="avaiamount">
                            </div>
                            <div id="avaiamountcontent"></div>
                            <?php echo form_error('avaiamount', '<div style="margin-left: 14%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                    </div>
                </div>
                <div>

                    <button id="myBtn" style="padding-left: 3%;padding-right: 3%;" type="submit"
                            class="btn btn-success">Insert
                    </button>
                    <div id="wrongmsg"></div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
 <?php  } ?>



<script>

    var pagevalue = 1;

    //load product data table function
    window.LoadProductTable = function (page, searchdata) {
        $.ajax({
            url: "<?php echo base_url();?>index.php/ProductStockManage/LoadProductDataTable/" + page,
            methode: "POST",
            dataType: "json",
            data: {"querydata": searchdata},
            success: function (data) {
                console.log(data)
                $('#ProductMaterial_table').html(data.producttable);
                $('#pagination_link').html(data.paginationdata);
            }, error: function (data) {
                console.log(data);
                alert("Oops! something went Wrong");
            }
        });
    }


    $(document).ready(function () {
        //calling load data to table function
        LoadProductTable(pagevalue, '');

        //pagination click events
        $(document).on("click", ".pagination li a", function (event) {
            event.preventDefault();
            pagevalue = $(this).data("ci-pagination-page");
            var search = document.getElementById("search_text").value;
            console.log(search);
            LoadProductTable(pagevalue, search);
        });

        //search input click events
        $('#search_text').keyup(function () {
            var search = $(this).val();
            if (search != '') {
                LoadProductTable(pagevalue, search);
            }
            else {
                LoadProductTable(pagevalue, '');
            }
        });


    });

    //Delete Product
    function DeleteProductData(id) {

        $(document).ready(function () {
            var pid = id;
            swal({
                title: "Are you sure?",
                text: "Do yo want to Delete this Product!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    url: "<?php echo base_url();?>index.php/ProductStockManage/DeleteProduct",
                    methode: "POST",
                    dataType: "json",
                    data: {"productID": pid},
                    success: function (data) {
                        swal("Deleted!", "Selected Product deleted.", "success");
                        LoadProductTable(pagevalue, '');

                    }, error: function (data) {
                        alert("Oops! something went Wrong");
                    }
                });


            });
        });

    }
</script>

