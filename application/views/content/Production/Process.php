<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Production Managment</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Production Processing</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10"><i
                class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="card">
    <div style="color: white" class="card-body">
        <h4 class="card-title">Manage Production proccesing</h4>
        <div class="table-responsive m-t-40">
            <div id="example23_wrapper" class="dataTables_wrapper">
                <!--                <div class="dt-buttons">-->
                <!--                    <form method="get" action="--><?php //base_url(); ?><!--ExportAsExcel">-->
                <!--                        <input style="border-color: transparent; cursor: pointer;" value="Excel" type="submit"-->
                <!--                               name="export" class="dt-button buttons-excel buttons-html5" tabindex="0"-->
                <!--                               aria-controls="example23">-->
                <!--                    </form>-->
                <!--                </div>-->
                <div id="example23_filter" class="dataTables_filter">
                    <label>Sort By Insert Date:<input style="color: white;" id="search_text" type="search" class="" placeholder=""
                                                      aria-controls="example23"></label>
                </div>
                <div id="productionprocess_table" class="dataTables_wrapper"></div>
                <div align="right" id="pagination_link"></div>
            </div>
        </div>
    </div>
</div>
</div>


<script>

    var pagevalue = 1;


    //load Production process data table function
    window.LoadProductionProcessTable = function (page, searchdata) {
        $.ajax({
            url: "<?php echo base_url();?>index.php/ProductionManage/LoadProductionProcessDataTable/" + page,
            methode: "POST",
            dataType: "json",
            data: {"querydata": searchdata},
            success: function (data) {
                console.log(data)
                $('#productionprocess_table').html(data.ProductionProcesstable);
                $('#pagination_link').html(data.paginationdata);
            }, error: function (data) {
                alert("Oops! something went Wrong");
            }
        });
    }


    $(document).ready(function () {
        //calling load data to table function
        LoadProductionProcessTable(pagevalue, '');

        //pagination click events
        $(document).on("click", ".pagination li a", function (event) {
            event.preventDefault();
            pagevalue = $(this).data("ci-pagination-page");
            var search = document.getElementById("search_text").value;
            console.log(search);
            LoadProductionProcessTable(pagevalue, search);
        });

        //search input click events
        $('#search_text').keyup(function () {
            var search = $(this).val();
            if (search != '') {
                LoadProductionProcessTable(pagevalue, search);
            }
            else {
                LoadProductionProcessTable(pagevalue, '');
            }
        });


    });
</script>