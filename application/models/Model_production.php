<?php

/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 23/11/2018
 * Time: 08:17
 */
class Model_production extends CI_Model
{
    //Getting perticular value from entered db table
    function DataRetrive($datatable, $feildvalue, $value, $returnfield)
    {

        $data = "";
        $sql = "SELECT * FROM " . $datatable . " WHERE " . $feildvalue . "=? AND isDeleted=0";
        $query = $this->db->query($sql, array($value));
        foreach ($query->result() as $row) {
            $data = $row->$returnfield;
        }

        return $data;
    }
//---------------------------------------------------Production request section-----------------------------------------------------------

    //fetching production data for table
    function FetchingProductionRequestDataForTable($limit, $start,$searchquery)
    {
        $output = '';
        $this->db->select("*");
        $this->db->from("productionrequest");
        $this->db->where("isDeleted","0");
        $this->db->where("state","1");
        if ($searchquery!=''){
            $this->db->like('insertdate', $searchquery);

        }
        $this->db->order_by("PRID");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if($query->num_rows()!=0) {
            $output .= '
        <table class="display nowrap table table-hover table-striped table-bordered dataTable"
           cellspacing="0" width="100%" role="grid" aria-describedby="example23_info" style="width: 100%;">
        <thead>
        <tr role="row">
             <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Age: activate to sort column ascending" style="width: 67px;">Order ID
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Age: activate to sort column ascending" style="width: 67px;">Insert Date
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending"
                aria-label="Name: activate to sort column descending" style="width: 175px;">Product Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Position: activate to sort column ascending" style="width: 254px;">Request Quantity
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Office: activate to sort column ascending" style="width: 133px;">Due Date
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Start date: activate to sort column ascending" style="width: 127px;">Action
            </th>
        </tr>
        </thead>
            ';
            foreach ($query->result() as $row) {
                $productname=$this->DataRetrive('products', 'PID', $row->productid, 'productname');

                $output .= '
           <tr>
           <td>' . $row->oderID . '</td>
             <td>' . $row->insertdate . '</td>
             <td>' . $productname . '</td>
             <td>' . $row->requestquantity . '</td>
             <td>' . $row->duedate . '</td>    
             <td class="text-nowrap">
                <div style=" margin-left: 20%;">
                    <a style=" margin-left: -11%;"  href=\''.base_url()."index.php/ProductionManage/SendToProcess?id=$row->PRID".'\'   role="button" class="btn btn-outline-success"> Begin Process </a>               
                 </div>         
             </td>
           </tr>
           ';
            }
            $output .= '</table>';
        }else{
            $output .= '<tr>
       <td colspan="5">No Data Found</td>
      </tr>
      
      ';
        }
        return $output;
    }

    //count all production requests for pagination function
    function count_all()
    {
        $this->db->select("*");
        $this->db->from("productionrequest");
        $this->db->where("isDeleted", "0");
        $this->db->where("state","1");
        $query = $this->db->get();
        return $query->num_rows();
    }


    //Get production request data related to the id
    public function GetProductionRequestData($id)
    {
        $this->db->select("*");
        $this->db->from("productionrequest");
        $this->db->where("PRID", $id);
        $result = $this->db->get();
        return $result->result();
    }

    //insert raw materials usage for order products
    public function InsertRawMatUsageForOrder($rawmaterials,$requireamount,$oid)
    {
        $date = date('Y-m-d H:i:s');
       for($i=0;$i<sizeof($rawmaterials);$i++)
       {
           $rawmatcriticalamnt=$this->DataRetrive("rawmaterials","RID",$rawmaterials[$i],"criticalamount");
           $rawmatavailableamnt=$this->DataRetrive("rawmaterials","RID",$rawmaterials[$i],"currentamount");
           $rawmatname=$this->DataRetrive("rawmaterials","RID",$rawmaterials[$i],"Rname");


           if($rawmatavailableamnt-$requireamount[$i]<0)
           {
               return 1; //some raw matirial out of stock
           }else{
               $currentamount=$rawmatavailableamnt-$requireamount[$i];
               if($currentamount==0 || $currentamount<$rawmatcriticalamnt)
               {

                   $NotificationHeader = "Raw material Out Stock";
                   $NotificationBody = $rawmatname . " Out Of stock";

                   $sqlnotification = "INSERT INTO Notification (NotificationHeader,NotificationBody,notificationtype,NotificationDate,NotificationStatus) values (?,?,'raw',?,0) ";
                   $this->db->query($sqlnotification, array($NotificationHeader, $NotificationBody, $date));

               }

               $sqlrawmaterial = "UPDATE rawmaterials SET currentamount=? WHERE RID=? ";
               $this->db->query($sqlrawmaterial, array($currentamount, $rawmatavailableamnt[$i]));

               $sqlorderrawmatusage = "INSERT INTO orderrawmatusage (OID,RID,useamount) values (?,?,?) ";
               $this->db->query($sqlorderrawmatusage, array($oid, $rawmaterials[$i], $requireamount[$i]));

               $sqlproductionrequest = "UPDATE productionrequest SET state=2 WHERE oderID=? ";
               $this->db->query($sqlproductionrequest, array($oid));


                return 2; //raw material deducted successfully
           }
       }
    }

    //---------------------------------------------production process section---------------------------------------------------

    public function FetchingProductionProcessDataForTable($limit, $start,$searchquery)
    {
        $output = '';
        $this->db->select("*");
        $this->db->from("productionrequest");
        $this->db->where("isDeleted","0");
        $this->db->where("state","2");
        if ($searchquery!=''){
            $this->db->like('insertdate', $searchquery);

        }
        $this->db->order_by("PRID");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if($query->num_rows()!=0) {
            $output .= '
        <table class="display nowrap table table-hover table-striped table-bordered dataTable"
           cellspacing="0" width="100%" role="grid" aria-describedby="example23_info" style="width: 100%;">
        <thead>
        <tr role="row">
             <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Age: activate to sort column ascending" style="width: 67px;">Order ID
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Age: activate to sort column ascending" style="width: 67px;">Insert Date
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending"
                aria-label="Name: activate to sort column descending" style="width: 175px;">Product Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Position: activate to sort column ascending" style="width: 254px;">Request Quantity
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Office: activate to sort column ascending" style="width: 133px;">Due Date
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Start date: activate to sort column ascending" style="width: 127px;">Action
            </th>
        </tr>
        </thead>
            ';
            foreach ($query->result() as $row) {
                $productname=$this->DataRetrive('products', 'PID', $row->productid, 'productname');

                $output .= '
           <tr>
           <td>' . $row->oderID . '</td>
             <td>' . $row->insertdate . '</td>
             <td>' . $productname . '</td>
             <td>' . $row->requestquantity . '</td>
             <td>' . $row->duedate . '</td>    
             <td class="text-nowrap">
                <div style=" margin-left: 20%;">
                    <a style=" margin-left: -11%;"  href=\''.base_url()."index.php/ProductionManage/SendToDone?id=$row->PRID".'\'   role="button" class="btn btn-outline-success"> Done </a>               
                 </div>         
             </td>
           </tr>
           ';
            }
            $output .= '</table>';
        }else{
            $output .= '<tr>
       <td colspan="5">No Data Found</td>
      </tr>
      
      ';
        }
        return $output;

    }

    public function FetchingProductionDoneDataForTable($limit, $start,$searchquery)
    {
        $output = '';
        $this->db->select("*");
        $this->db->from("productionrequest");
        $this->db->where("isDeleted","0");
        $this->db->where("state","3");
        if ($searchquery!=''){
            $this->db->like('insertdate', $searchquery);

        }
        $this->db->order_by("PRID");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if($query->num_rows()!=0) {
            $output .= '
        <table class="display nowrap table table-hover table-striped table-bordered dataTable"
           cellspacing="0" width="100%" role="grid" aria-describedby="example23_info" style="width: 100%;">
        <thead>
        <tr role="row">
             <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Age: activate to sort column ascending" style="width: 67px;">Order ID
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Age: activate to sort column ascending" style="width: 67px;">Insert Date
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending"
                aria-label="Name: activate to sort column descending" style="width: 175px;">Product Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Position: activate to sort column ascending" style="width: 254px;">Request Quantity
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Office: activate to sort column ascending" style="width: 133px;">Due Date
            </th>

        </tr>
        </thead>
            ';
            foreach ($query->result() as $row) {
                $productname=$this->DataRetrive('products', 'PID', $row->productid, 'productname');

                $output .= '
           <tr>
           <td>' . $row->oderID . '</td>
             <td>' . $row->insertdate . '</td>
             <td>' . $productname . '</td>
             <td>' . $row->requestquantity . '</td>
             <td>' . $row->duedate . '</td>    
           </tr>
           ';
            }
            $output .= '</table>';
        }else{
            $output .= '<tr>
       <td colspan="5">No Data Found</td>
      </tr>
      
      ';
        }
        return $output;

    }



    public function countprocess_all()
    {
        $this->db->select("*");
        $this->db->from("productionrequest");
        $this->db->where("isDeleted", "0");
        $this->db->where("state","2");
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function countdone_all()
    {
        $this->db->select("*");
        $this->db->from("productionrequest");
        $this->db->where("isDeleted", "0");
        $this->db->where("state","3");
        $query = $this->db->get();
        return $query->num_rows();
    }

    public  function SetTodone($id)
    {
        $sqlrawmaterial = "UPDATE productionrequest SET state=3 WHERE PRID=? ";
        $result=$this->db->query($sqlrawmaterial, array($id));
        return $result;
    }

}