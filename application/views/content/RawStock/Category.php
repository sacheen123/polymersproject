<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Raw Material Stock Managment</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Category Manage</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10"><i
                    class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<?php foreach ($currentdata as $d) {
    if ($d->cid != '') { ?>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div style="color: white;padding: 17px" class="card-body">
                        <h4 class="card-title">View Categories</h4>
                        <div class="table-responsive m-t-40">
                            <div id="example23_wrapper" class="dataTables_wrapper">
                                <div id="example23_filter" class="dataTables_filter">
                                    <label>Search:<input style="color: white;" id="search_text" type="search" class=""
                                                         placeholder="" aria-controls="example23"></label>
                                </div>

                                <div id="category_table" class="dataTables_wrapper"></div>
                                <div align="right" id="pagination_link"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div style="color: white" class="card-body">
                        <h4 class="card-title">Update Category</h4>
                        <?php $data = array('onsubmit' => 'return validateRequirecategory()', 'name' => 'categoryAddForm'); ?>
                        <?php echo form_open('RawStockManage/UpdateCategory?cid=' . $d->cid, $data); ?>
                        <div>
                            <div class="form-group row">

                                <div class="col-10">
                                    <label style="color: white" for="example-text-input" class=" col-form-label">Category
                                        Name</label>
                                    <input style=" text-transform: uppercase;color:white;" autocomplete="off"
                                           placeholder="Ex: PVC SOCKETS" style="color: white" class="form-control"
                                           type="text" value="<?php echo $d->cname ?>" name="cname">
                                </div>
                                <div id="cnamecontent"></div>
                                <?php echo form_error('cname', '<div style="margin-left: 3%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                            </div>
                            <div class="form-group row">
                                <div class="col-10">
                                    <label style="color: white" for="example-email-input" class=" col-form-label">Mesurment
                                        Type <?php echo $d->mtype; ?></label>
                                    <select style="color: white;" name="mtype" class="form-control" id="sel1">
                                        <?php
                                        foreach ($mesurments as $s) { ?>
                                            <option  <?= $d->mtype ==  $s->mid ? ' selected="selected"' : '';?>   value="<?php echo $s->mid;?>"><?php echo $s->mesurement; ?></option>';
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                                <div id="passwordcontent"></div>
                                <?php echo form_error('password', '<div style="margin-left: 3%;margin-top: 0.3%;" class=" tst4 btn btn-danger">', '</div>'); ?>
                            </div>
                            <div>
                                <div class="row">
                                    <div class="col-md-7">
                                        <button id="myBtn" style="padding-left: 3%;padding-right: 3%;" type="submit"
                                                class="btn btn-success">update
                                        </button>
                                        <a role="button" href="<?php echo base_url() ?>index.php/RawStockManage/Category"
                                           id="myBtn" style="padding-left: 3%;padding-right: 3%;" type="submit"
                                           class="btn btn-danger">Back </a>
                                    </div>
                                    <div class="col-md-5">

                                    </div>
                                </div>

                                <div style="margin-left: 12%;" id="wrongmsg"></div>
                            </div>
                        </div>

                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    <?php } else { ?>
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div style="color: white;padding: 17px" class="card-body">
                        <h4 class="card-title">View Categories</h4>
                        <div class="table-responsive m-t-40">
                            <div id="example23_wrapper" class="dataTables_wrapper">
                                <div id="example23_filter" class="dataTables_filter">
                                    <label>Search:<input style="color: white;" id="search_text" type="search" class=""
                                                         placeholder="" aria-controls="example23"></label>
                                </div>
                                <div id="category_table" class="dataTables_wrapper"></div>
                                <div align="right" id="pagination_link"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div style="color: white" class="card-body">
                        <h4 class="card-title">Insert Category</h4>
                        <?php $data = array('onsubmit' => 'return validateRequirecategory()', 'name' => 'categoryAddForm'); ?>
                            <?php echo form_open('RawStockManage/InsertCategory', $data); ?>
                        <div class="row">
                            <div class="col-md-6" >
                                <div class="form-group row">
                                    <div class="col-10">
                                        <label style="color: white" for="example-text-input" class=" col-form-label">Category
                                            Name</label>
                                        <input style=" text-transform: uppercase;color:white;" autocomplete="off"
                                               placeholder="Ex: PVC SOCKETS" style="color: white" class="form-control"
                                               type="text" value="" name="cname">
                                    </div>
                                    <div id="cnamecontent"></div>
                                    <?php echo form_error('cname', '<div style="margin-left: 3%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="col-md-6" >
                                <div class="form-group row">
                                    <div class="col-10">
                                        <label style="color: white" for="example-email-input" class=" col-form-label">Mesurment
                                            Type</label>
                                        <select style="color: white;" name="mtype" class="form-control" id="sel1">
                                            <?php
                                            foreach ($mesurments as $row) { ?>
                                                <option   value="<?php echo $row->mid;?>"><?php echo $row->mesurement; ?></option>';
                                            <?php }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <button id="myBtn"  type="submit"
                                    class="btn btn-success">Insert
                            </button>
                            <div  id="wrongmsg"></div>
                        </div>

                        <?php echo form_close(); ?>
                    </div>
                </div>

                <div class="card">
                    <div style="color: white" class="card-body">
                        <h4 class="card-title">Insert New Measurment type</h4>
<!--                        --><?php //$data = array('onsubmit' => 'return validateRequirecategory()', 'name' => 'categoryAddForm'); ?>
                        <?php echo form_open('RawStockManage/InsertMeasurment'); ?>
                        <div class="row">
                            <div class="col-md-9" >
                                <div class="form-group row">
                                    <div class="col-10">
                                        <label style="color: white" for="example-text-input" class=" col-form-label">Measurment
                                        </label>
                                        <input style=" text-transform: uppercase;color:white;" autocomplete="off"
                                               placeholder="Ex:Kg" style="color: white" class="form-control"
                                               type="text" value="" name="mesname">
                                    </div>
                                    <div id="mesecontent"></div>
                                    <?php echo form_error('mesname', '<div style="margin-left: 3%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                                </div>
                            </div>
                            <div class="col-md-3" >
                                    <button style=" margin-top: 39%;" id="mymesBtn"  type="submit"
                                            class="btn btn-success">Insert
                                    </button>
                                    <div  id="wrongmsg"></div>
                            </div>
                        </div>


                        <?php echo form_close(); ?>
                        <div>
                            <strong>Measurment List :   </strong>
                            <?php foreach ($mesurments as $row){ ?><?php echo  $row->mesurement;?> , <?php } ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    <?php } ?>


    </div>
<?php } ?>

<script>
    //Delete category
    function DeleteCategory(id) {

        $(document).ready(function () {
            var cid = id;
            swal({
                title: "Are you sure?",
                text: "Do yo want to Delete this Category!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    url: "<?php echo base_url();?>index.php/RawStockManage/DeleteCategory",
                    methode: "POST",
                    dataType: "json",
                    data: {"categoryID": cid},
                    success: function (data) {
                        swal("Deleted!", "Selected Category deleted.", "success");
                        LoadCategoryTable(pagevalue, '');

                    }, error: function (data) {
                        alert("Oops! something went Wrong");
                    }
                });


            });
        });

    }


    var pagevalue = 1;

    //load category data table function
    window.LoadCategoryTable = function (page, searchdata) {
        $.ajax({
            url: "<?php echo base_url();?>index.php/RawStockManage/LoadCategoryDataTable/" + page,
            methode: "POST",
            dataType: "json",
            data: {"querydata": searchdata},
            success: function (data) {
                console.log(data)
                $('#category_table').html(data.categorytable);
                $('#pagination_link').html(data.paginationdata);
            }, error: function (data) {
                console.log(data);
                alert("Oops! something went Wrong");
            }
        });
    }


    $(document).ready(function () {
        //calling load data to table function
        LoadCategoryTable(pagevalue, '');

        //pagination click events
        $(document).on("click", ".pagination li a", function (event) {
            event.preventDefault();
            pagevalue = $(this).data("ci-pagination-page");
            var search = document.getElementById("search_text").value;
            console.log(search);
            LoadCategoryTable(pagevalue, search);
        });

        //search input click events
        $('#search_text').keyup(function () {
            var search = $(this).val();
            if (search != '') {
                LoadCategoryTable(pagevalue, search);
            }
            else {
                LoadCategoryTable(pagevalue, '');
            }
        });


    });
</script>


