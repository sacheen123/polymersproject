<?php

/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 13/10/2018
 * Time: 13:35
 */
class UserProfileManage extends CI_Controller
{
    public $data=array('subview'=>'Oops subview not set','currentdata'=>'');

    //Loading Userprofile managemnt ui
    public function index()
    {
        $this->GetCurrentUserData();
        $this->data['subview']='ManageProfile';
        $this->load->view('Home',$this->data);
    }

    //Set current user data in topheader
    public function  GetCurrentUserData()
    {
      $this->load->model('Model_user');
      $result=$this->Model_user->GetUserData();
      $this->data['currentdata']=$result;

    }


    //Update user profile data
    public  function UpdateUserProfile(){

        $this->form_validation->set_rules('uname', 'User Name', 'required');
        $this->form_validation->set_rules('fname', 'First Name', 'required');
        $this->form_validation->set_rules('lname', 'Last Name', 'required');
        $this->form_validation->set_rules('pnumber', 'Phone Number', 'required|regex_match[/^[0-9]{9}$/]');

        if ($this->form_validation->run() == FALSE)
        {
            $this->index();
        }else{
            $this->load->model('Model_user');
            $result= $this->Model_user->UpdateUserData();

            if($result=='1'){
                $this->session->set_flashdata('msgS','Successfully Updated!');
                redirect('UserProfileManage/index');
            }else{
                $this->session->set_flashdata('msgW','Something went Wrong!!');
                redirect('UserProfileManage/index');
            }
        }

    }


    //Update user password
    public  function UpdateUserPassword(){

        $this->form_validation->set_rules('cpwd', 'Current Password', 'required');
        $this->form_validation->set_rules('npwd', 'New Password', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->index();
        }else{
            $this->load->model('Model_user');
            $result= $this->Model_user->UpdatePassword();
            echo $result;

            if($result=='True'){
                $this->session->set_flashdata('msgS','Password Successfully Updated!');
                redirect('UserProfileManage/index');
            }else{
                $this->session->set_flashdata('msgW','Entered current Password Not Matched.Check again!!');
                redirect('UserProfileManage/index');
            }
        }

    }







}