<?php

/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 14/10/2018
 * Time: 16:15
 */
class UsersManage extends CI_Controller
{
    public $data=array('subview'=>'Oops subview not set','currentdata'=>'');
    //Load Add user UI
    public function AddUser()
    {
        $this->GetCurrentUserData();
        $this->data['subview']='Users/AddUser';
        $this->load->view('Home',$this->data);
    }
    //Load View User UI
    public function ViewUser()
    {
        $this->GetCurrentUserData();
        $this->data['subview']='Users/ViewUsers';
        $this->load->view('Home',$this->data);
    }

    //Load Edit User UI
    public function EditUser()
    {
        $id = $this->input->get('id');
        $this->load->model('Model_user');
        $dataset=$this->Model_user->GetRequestedUserData($id);
        $this->data['currentdata']=$dataset;
        $this->data['subview']='Users/EditUser';
        $this->load->view('Home',$this->data);

    }

    //Edit currentAvailable user data
    public  function EditcurrentUser(){
        $id = $this->input->get('id');

        $this->form_validation->set_rules('fname', 'First Name', 'required');
        $this->form_validation->set_rules('lname', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('pnumber', 'Phone Number', 'required|regex_match[/^[0-9]{9}$/]');
        if ($this->form_validation->run() == FALSE)
        {
            redirect('UsersManage/EditUser?id='.$id);
        }else{
            $this->load->model('Model_user');
            $result= $this->Model_user->UpdateCurrentUser($id);
            if($result=='1') {
                $this->session->set_flashdata('msgS', 'User Successfully Updated !!');
                redirect('UsersManage/ViewUser');
            }
        }
    }


    //Manage data in viewuser table
    Public function LoadUserDataTable()
    {
        $query = '';
        $this->load->model('Model_user');
        $this->load->library("pagination");
        $config = array();
        $config["base_url"] = "#";
        $config["total_rows"] = $this->Model_user->count_all();
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li>';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li>';
        $config["last_tag_close"] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>Previous Page';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li>";
        $config["num_tag_close"] = "</li>";
        $config["num_links"] = 1;

        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $start = ($page - 1) * $config["per_page"];
        if ($this->input->get('querydata'))
        {
            $query = $this->input->get('querydata');
        }

        $output=array(
          'paginationdata' => $this->pagination->create_links(),
          'usertable'  => $this->Model_user->FetchingUserDataForTable($config["per_page"],$start, $query)
        );

        echo json_encode($output);
    }

    //Making active user inactive
    public  function MakeInactiveUser(){
        $id = $this->input->get('inactiveID');
        $this->load->model('Model_user');
        $result=$this->Model_user->SetInactiveUser($id);
        $output=array('responce'=>$result);
        echo json_encode($output);

    }

    //Deleting User
    public function DeleteUser()
    {
        $id = $this->input->get('userID');
        $this->load->model('Model_user');
        $result=$this->Model_user->DeleteUser($id);
        $output=array('responce'=>$result);
        echo json_encode($output);
    }


   //Making Inactiveuser active
    public  function MakeactiveUser(){
        $id = $this->input->get('inactiveID');
        $this->load->model('Model_user');
        $result=$this->Model_user->SetactiveUser($id);
        $output=array('responce'=>$result);
        echo json_encode($output);

    }

    //Set current user data in topheader
    public function  GetCurrentUserData()
    {
        $this->load->model('Model_user');
        $result=$this->Model_user->GetUserData();
        $this->data['currentdata']=$result;

    }

    //Insert user data
    public  function InsertNewUser(){


        $this->form_validation->set_rules('uname', 'User Name', 'required');
        $this->form_validation->set_rules('fname', 'First Name', 'required');
        $this->form_validation->set_rules('lname', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('cpassword', 'confirm Password', 'required');
        $this->form_validation->set_rules('pnumber', 'Phone Number', 'required|regex_match[/^[0-9]{9}$/]');

        if ($this->form_validation->run() == FALSE)
        {
            $this->AddUser();
        }else{
            $this->load->model('Model_user');
            $result= $this->Model_user->InsertNewUser();
            if($result==3){
                $this->session->set_flashdata('msgS','User Successfully Inserted !!');
                redirect('UsersManage/AddUser');
            }elseif ($result==1){
                $this->session->set_flashdata('msgW','Username Allready Available!!');
                redirect('UsersManage/AddUser');
            }elseif ($result==2){
                $this->session->set_flashdata('msgW','Password and Confirm Passwords Not Matching !!');
                redirect('UsersManage/AddUser');
            }else{
                $this->session->set_flashdata('msgW','Something went wrong !!');
                redirect('UsersManage/AddUser');
            }
        }
    }

    //Export Excel
    public function ExportAsExcel()
    {
        $this->load->model('Model_user');
        $userdata=$this->Model_user->Fetchforexcel();
        $this->load->library("excel");
        $object=new PHPExcel();
        $object->setActiveSheetIndex(0);
        $table_columns=array("username","fname","lname","email","PhoneNumber","createDate");
        $column=0;
        foreach ($table_columns as $field)
        {
            $object->getActiveSheet()->setCellValueByColumnAndRow($column,1,$field);
            $column++;
        }

        $excel_row=2;
        foreach ($userdata as $row)
        {
            $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->username);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->fname);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->lname);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->email);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$row->PhoneNumber);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5,$excel_row,$row->createDate);
            $excel_row++;
        }

        $object_writer=PHPExcel_IOFactory::createWriter($object,'Excel5');
        header('Content-Type:application/vnd.ms-excel');
        header('Content-Disposition:attachment;filename="User-Data.xls"');
        $object_writer->save('php://output');
    }
}