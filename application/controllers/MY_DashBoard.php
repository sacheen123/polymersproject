<?php

/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 15/08/2018
 * Time: 21:25
 */
class MY_DashBoard extends CI_Controller
{
    public $data=array('subview'=>'Oops subview not set','currentdata'=>'','notification'=>'');

    //Set current user data in topheader
    public function  GetCurrentUserData()
    {
        $this->load->model('Model_user');
        $result=$this->Model_user->GetUserData();
        $this->data['currentdata']=$result;
    }

    //Loading Dashboard ui
    public function index()
    {
        $this->load->model('Model_user');
        $resultnot=$this->Model_user->GetNotifications();
        $this->GetCurrentUserData();
        $this->data['notification']=$resultnot;
        $this->data['subview']='WelcomeDashBoard';
        $this->load->view('Home',$this->data);
    }






}