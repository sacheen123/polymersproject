/**
 * Created by sacheen on 23/10/2018.
 */

//Check only for numeric values
function allnumeric(inputtxt)
{
    var numbers = /^[0-9]+$/;
    if(inputtxt.value.match(numbers))
    {
        return 1;
    }
    else
    {
        return 2;
    }
}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//compare input and confirm password
function CheckInputPassword () {
    var Epassword = document.getElementById("password").value;
    var confirmpassword = document.getElementById("cpassword").value;

    if (Epassword != confirmpassword) {
        document.getElementById("content").innerHTML='<div style="margin-left: 8%;margin-top: 0.3%;" class=" tst4 btn btn-danger">Passwords Not Matched</div>';
        document.getElementById("myBtn").disabled = true;
        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style=" margin-top: -2.5%; margin-left: 11%;color: red;">Correct the Invalid Fields</h5></strong>';
    }else{
        document.getElementById("content").innerHTML='';
        document.getElementById("myBtn").disabled = false;
        document.getElementById("wrongmsg").innerHTML = '';
    }

}

function ValidateEmail(mail)
{
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
    {
        document.getElementById("emailcontent").innerHTML='';
        document.getElementById("myBtn").disabled = false;
        document.getElementById("wrongmsg").innerHTML = '';

    }else{
        document.getElementById("emailcontent").innerHTML='<div style="margin-left: 15%;margin-top: 0.3%;" class=" tst4 btn btn-danger">Invalid Email</div>';
        document.getElementById("myBtn").disabled = true;
        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style=" margin-top: -2.5%; margin-left: 11%;color: red;">Correct the Invalid Fields</h5></strong>';
    }
}
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//validations in product insert and update form
function validateRequireproduct()
{
    var pname = document.forms["productForm"]["pname"].value;
    var unitprice = document.forms["productForm"]["unitprice"].value;
    var criamount = document.forms["productForm"]["criamount"].value;
    var availableamount = document.forms["productForm"]["avaiamount"].value;

    document.getElementById("pnamecontent").innerHTML='<div ></div>';
    document.getElementById("unitpricecontent").innerHTML='<div ></div>';
    document.getElementById("criamountcontent").innerHTML='<div ></div>';
    document.getElementById("avaiamountcontent").innerHTML='<div ></div>';
    if (pname == "" ) {

        document.getElementById("pnamecontent").innerHTML='<div style="margin-left: 9%;margin-top: 0.3%;" class=" tst4 btn btn-danger">Product Name Field Required</div>';
        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style="margin-top: -6.5%; margin-left: 20%; color: red;">Correct the Invalid Fields</h5></strong>';
        return false;
    }
    if (unitprice == "" ) {

        document.getElementById("unitpricecontent").innerHTML='<div style="margin-left: 9%;margin-top: 0.3%;" class=" tst4 btn btn-danger">Unit Price Field Required</div>';
        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style="margin-top: -6.5%; margin-left: 20%; color: red;">Correct the Invalid Fields</h5></strong>';
        return false;
    }

    if (criamount == "" ) {

        document.getElementById("criamountcontent").innerHTML='<div style="margin-left: 9%;margin-top: 0.3%;" class=" tst4 btn btn-danger">Critical Field Required</div>';
        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style="margin-top: -6.5%; margin-left: 20%; color: red;">Correct the Invalid Fields</h5></strong>';
        return false;
    }

    if (availableamount == "" ) {

        document.getElementById("avaiamountcontent").innerHTML='<div style="margin-left: 9%;margin-top: 0.3%;" class=" tst4 btn btn-danger">Available amount Field Required</div>';
        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style="margin-top: -6.5%; margin-left: 20%; color: red;">Correct the Invalid Fields</h5></strong>';
        return false;
    }

    document.getElementById("wrongmsg").innerHTML = '';
    return true;

}

//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//validattion for category insert and updateform
function validateRequirecategory()
{

    var cname = document.forms["categoryAddForm"]["cname"].value;
    if (cname == "" ) {

        document.getElementById("cnamecontent").innerHTML='<div style="margin-left: 9%;margin-top: 0.3%;" class=" tst4 btn btn-danger">Category Name Field Required</div>';
        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style="     margin-top: -6.5%; margin-left: 20%; color: red;;">Correct the Invalid Fields</h5></strong>';
        return false;
    }
    document.getElementById("wrongmsg").innerHTML = '';
    return true;

}

//validation in user insert
function validateRequire() {
    var fname = document.forms["UserAddForm"]["fname"].value;
    var lname = document.forms["UserAddForm"]["lname"].value;
    var uname = document.forms["UserAddForm"]["uname"].value;
    var password = document.forms["UserAddForm"]["password"].value;
    var cpassword=document.forms["UserAddForm"]["cpassword"].value;
    var email=document.forms["UserAddForm"]["email"].value;
    var pnumber=document.forms["UserAddForm"]["pnumber"].value;

    if (fname == "" ) {

        document.getElementById("fnamecontent").innerHTML='<div style="margin-left: 9%;margin-top: 0.3%;" class=" tst4 btn btn-danger">First Name Field Required</div>';
        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style=" margin-top: -2.5%; margin-left: 11%;color: red;">Correct the Invalid Fields</h5></strong>';
        return false;
    }
    if (lname == "") {
        document.getElementById("lnamecontent").innerHTML='<div style="margin-left: 9%;margin-top: 0.3%;" class=" tst4 btn btn-danger">Last Name Field Required</div>';
        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style=" margin-top: -2.5%; margin-left: 11%;color: red;">Correct the Invalid Fields</h5></strong>';
        document.getElementById("fnamecontent").innerHTML='';
        return false;
    }
    if (uname == "" ) {
        document.getElementById("usernamecontent").innerHTML='<div style="margin-left: 9%;margin-top: 0.3%;" class=" tst4 btn btn-danger">User Name Field Required</div>';
        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style=" margin-top: -2.5%; margin-left: 11%;color: red;">Correct the Invalid Fields</h5></strong>';
        document.getElementById("lnamecontent").innerHTML='';
        return false;
    }
    if (password == "") {
        document.getElementById("passwordcontent").innerHTML='<div style="margin-left: 9%;margin-top: 0.3%;" class=" tst4 btn btn-danger">Password Field Required</div>';
        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style=" margin-top: -2.5%; margin-left: 11%;color: red;">Correct the Invalid Fields</h5></strong>';
        document.getElementById("usernamecontent").innerHTML='';
        return false;
    }
    if (cpassword==""){
        document.getElementById("confirmpasswordcontent").innerHTML='<div style="margin-left: 9%;margin-top: 0.3%;" class=" tst4 btn btn-danger">Confirm Password Field Required</div>';
        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style=" margin-top: -2.5%; margin-left: 11%;color: red;">Correct the Invalid Fields</h5></strong>';
        document.getElementById("passwordcontent").innerHTML='';
        return false;
    }
    if (email==""){
        document.getElementById("emailcontent").innerHTML='<div style="margin-left: 9%;margin-top: 0.3%;" class=" tst4 btn btn-danger">Email Field Required</div>';
        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style=" margin-top: -2.5%; margin-left: 11%;color: red;">Correct the Invalid Fields</h5></strong>';
        document.getElementById("confirmpasswordcontent").innerHTML='';
    }
    if (pnumber==""){
        document.getElementById("phonecontent").innerHTML='<div style="margin-left: 9%;margin-top: 0.3%;" class=" tst4 btn btn-danger">Phone Number Field Required</div>';
        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style=" margin-top: -2.5%; margin-left: 11%;color: red;">Correct the Invalid Fields</h5></strong>';
        document.getElementById("emailcontent").innerHTML='';
    }

    document.getElementById("wrongmsg").innerHTML = '';
    return true;

}

//------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


//validate order insetr form
 function delivarydatechecking()
 {

     var date1=document.getElementById("orderdate").value;
     var date2=document.getElementById("delivarydate").value;

     var yearDiff=date1-date2;// for year difference
     alert(yearDiff);


 }

 function validateOredrForm()
 {
     var cusname = document.forms["OredrAddForm"]["cusname"].value;
     var cusaddress = document.forms["OredrAddForm"]["cusaddress"].value;
     var quantity = document.forms["OredrAddForm"]["quantitiy"].value;

     document.getElementById("cusnamecontent").innerHTML='<div ></div>';
     document.getElementById("cusaddresscontent").innerHTML='<div ></div>';
     document.getElementById("quantitycontent").innerHTML='<div ></div>';

     if (cusname == "" ) {

         document.getElementById("cusnamecontent").innerHTML='<div style=" margin-left: 1%;margin-top: -5%;" class=" tst4 btn btn-danger">Customer name Field Required</div>';
         document.getElementById("wrongmsg").innerHTML = '<strong><h5 style="  margin-left: 48%;margin-top: -3%;color: red;">Correct the Invalid Fields</h5></strong>';
         return false;
     }

     if (cusaddress == "" ) {

         document.getElementById("cusaddresscontent").innerHTML='<div style=" margin-left: 1%;margin-top: -5%;" class=" tst4 btn btn-danger">Customer Adress Field Required</div>';
         document.getElementById("wrongmsg").innerHTML = '<strong><h5 style="  margin-left: 48%;margin-top: -3%;color: red;">Correct the Invalid Fields</h5></strong>';
         return false;
     }

     if (quantity == "" ) {

         document.getElementById("quantitycontent").innerHTML='<div style="margin-left: -33%; margin-top: 3%;" class=" tst4 btn btn-danger">Quantity Field Required</div>';
         document.getElementById("wrongmsg").innerHTML = '<strong><h5 style="  margin-left: 48%;margin-top: -3%;color: red;">Correct the Invalid Fields</h5></strong>';
         return false;
     }



     document.getElementById("wrongmsg").innerHTML = '';
     return true;
 }


