<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url()?>/assets/images/favicon.png">
    <title>Monster Admin Template - The Most Complete & Trusted Bootstrap 4 Admin Template</title>
    <!--alerts CSS -->
    <link href="<?php echo base_url()?>/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url()?>/assets/plugins/bootstrap/css/bootstrap1.min.css" rel="stylesheet">

    <!-- chartist CSS -->
    <link href="<?php echo base_url()?>/assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>/assets/plugins/chartist-js/dist/chartist-init.css" rel="stylesheet">
    <link href="<?php echo base_url()?>/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <link href="<?php echo base_url()?>/assets/plugins/css-chart/css-chart.css" rel="stylesheet">
    <link href="<?php echo base_url()?>/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
    <!-- Custom CSS -->
    <link href="<?php echo base_url()?>/assets/css/style.css" rel="stylesheet">
    <!-- You can change the theme colors from here -->
    <link href="<?php echo base_url()?>/assets/css/colors/megna-dark.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>


    <![endif]-->
    <!--alerts CSS -->
    <link href="<?php echo base_url()?>/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">

    <script src="<?php echo base_url()?>/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <script src="<?php echo base_url()?>/assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>
    <script src="<?php echo base_url()?>/assets/js/customfunctions.js"></script>
    <script src="<?php echo base_url()?>/assets/js/choosen.js"></script>
    <script src="<?php echo base_url()?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url()?>/assets/js/jquery-ui.min.js"></script>
    <script src="<?php echo base_url()?>/assets/js/jquery.datatable.min.js"></script>
    <script src="<?php echo base_url()?>/assets/js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>/assets/css/dataTables.bootstrap.min.css" />

    <style>
        .pagination{
            float: left;
            font:11px Tahoma, Verdana, Arial, "Trebuchet MS", Helvetica, sans-serif;
            color:#3d3d3d;
            margin-top: 20px;
            margin-left:auto;
            margin-right:auto;
            margin-bottom:20px;
            width:100%;
        }
        .pagination a, #pagination strong{
            list-style-type: none;
            display: inline;
            padding: 5px 8px;
            text-decoration: none;
            background-color: inherit;
            color: #EC7117;
            font-weight: bold;
        }
        .pagination strong{
            color: #ffffff;
            background-color:#F00;
            background-position: top center;
            background-repeat: no-repeat;
            text-decoration: none;
        }
        .pagination a:hover{
            color: #ffffff;
            background-color:#F00;
            background-position: top center;
            background-repeat: no-repeat;
            text-decoration: none;
        }
    </style>


</head>