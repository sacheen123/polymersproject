<?php

/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 19/11/2018
 * Time: 08:45
 */
class Model_productstock extends CI_Model
{
    //Getting perticular value from entered db table
    function DataRetrive($datatable, $feildvalue, $value, $returnfield)
    {

        $data = "";
        $sql = "SELECT * FROM " . $datatable . " WHERE " . $feildvalue . "=? AND isDeleted=0";
        $query = $this->db->query($sql, array($value));
        foreach ($query->result() as $row) {
            $data = $row->$returnfield;
        }

        return $data;
    }

//------------------------------------------------Products section-------------------------------------------------------------
   //inserting new product details to db
    public function InsertNewProductItem()
    {
        $pname = strtoupper($this->input->post('pname', TRUE));
        $price = $this->input->post('unitprice', TRUE);
        $criamount = $this->input->post('criamount', TRUE);
        $avaiamount = $this->input->post('avaiamount', TRUE);
        $date = date('Y-m-d H:i:s');
        $ProductNameAvailability = $this->DataRetrive(' products', 'productname', $pname, 'PID');
        if ($ProductNameAvailability == '') {
            if($avaiamount==$criamount || $avaiamount< $criamount)
            {
                //stock over give notification
                $NotificationHeader = "Product Out Stock";
                $NotificationBody = $pname . " Out Of stock";
                $sqlnotification = "INSERT INTO Notification (NotificationHeader,NotificationBody,notificationtype,NotificationDate,NotificationStatus) values (?,?,'product',?,0) ";
                $this->db->query($sqlnotification, array($NotificationHeader,$NotificationBody, $date));
            }
            $sql = "INSERT INTO products (productname,unitprice,criticalamount,availableamount,isDeleted,InsertDate) values (?,?,?,?,'0',?) ";
            $result = $this->db->query($sql, array($pname, $price,$criamount,$avaiamount, $date));
            if ($result == 1) {
                return 1;
            } else {
                return 3;
            }
        } else {
            return 2; //productname already available
        }
    }

    //count all Products for pagination function
    function count_all()
    {
        $this->db->select("*");
        $this->db->from("products");
        $this->db->where("isDeleted", "0");
        $query = $this->db->get();
        return $query->num_rows();
    }

    //fetching product data to product table
    public function FetchingProductDataForTable($limit, $start, $searchquery)
    {
        $output = '';
        $this->db->select("*");
        $this->db->from("products");
        $this->db->where("isDeleted", "0");
        if ($searchquery != '') {
            $this->db->like('productname', $searchquery);
        }
        $this->db->order_by("PID");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $output .= '
        <table class="display nowrap table table-hover table-striped table-bordered dataTable"
           cellspacing="0" width="100%" role="grid" aria-describedby="example23_info" style="width: 100%;">
        <thead>
        <tr role="row">
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending"
                aria-label="Name: activate to sort column descending" style="width: 175px;">Product Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Position: activate to sort column ascending" style="width: 254px;">Unit Price(Rs:)
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending"
                aria-label="Name: activate to sort column descending" style="width: 175px;">Critical Amount
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending"
                aria-label="Name: activate to sort column descending" style="width: 175px;">Available amount
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Office: activate to sort column ascending" style="width: 133px;">Action
            </th>
        </tr>
        </thead>
            ';
            foreach ($query->result() as $row) {



                $output .= '
           <tr>
             <td>' . $row->productname . '</td>
             <td>' . $row->unitprice . '</td>
             <td>' . $row->criticalamount . '</td>
             <td>' . $row->availableamount . '</td>
             <td class="text-nowrap">
                <div style=" margin-left: 20%;">
                    <a  href=\'' . base_url() . "index.php/ProductStockManage/EditProduct?id=" .$row->PID. '\'  data-toggle="tooltip" data-original-title="Edit"> <i  class="fa fa-pencil  m-r-10"></i> </a>
                    <a style="cursor: pointer;" onclick="DeleteProductData('.$row->PID.')"  data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>                 
                 </div>
                           
             </td>
           </tr>
           ';
            }
            $output .= '</table>';
        } else {
            $output .= '<tr>
       <td colspan="5">No Data Found</td>
      </tr>
      
      ';
        }
        return $output;
    }

    //delete selected product
    public function DeleteProduct($id)
    {
        $sql = "UPDATE products SET  isDeleted='1'  WHERE PID=? ";
        $this->db->query($sql, array($id));
        return 'true';
    }

    //Get requested product details according to id
    public function GetRequestedProductData($id)
    {
        $sql = "SELECT * FROM  products WHERE PID=?";
        $result = $this->db->query($sql, array($id));
        return $result->result();
    }

    //update product data
    public function UpdateCurrentProduct($id)
    {
        $pname = $this->input->post('pname', TRUE);;
        $unitprice = $this->input->post('unitprice', TRUE);
        $criamount = $this->input->post('criamount', TRUE);
        $avaiamount = $this->input->post('avaiamount', TRUE);

        $ProductNameAvailability = $this->DataRetrive('products', 'productname', $pname, 'PID');
        $productname=$this->DataRetrive('products', 'PID', $id, 'productname');
        if ($ProductNameAvailability!='' && $pname!=$productname){
            return 2;
        }else{
            $sql = "UPDATE products SET productname=?,unitprice=?,criticalamount=?,availableamount=? WHERE PID=? ";
            $result = $this->db->query($sql, array($pname, $unitprice,$criamount,$avaiamount, $id));
            return 1;
        }


    }
}