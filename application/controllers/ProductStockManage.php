<?php

/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 18/11/2018
 * Time: 19:23
 */
class ProductStockManage extends CI_Controller
{
    public $data=array('subview'=>'Oops subview not set','currentdata'=>'','categorylist'=>'','productlist'=>'','mesurments'=>'');

    //Set current user data in topheader
    public function  GetCurrentUserData()
    {
        $this->load->model('Model_user');
        $result=$this->Model_user->GetUserData();
        $this->data['currentdata']=$result;

    }

    //--------------------------------------------------------Products Manage section-------------------------------------------------

    //Load Products manage ui
    public function Products()
    {

        $this->GetCurrentUserData();
        $this->data['subview']='ProductStock/Products';
        $this->load->view('Home',$this->data);
    }

    //Insert product details
    public function InsertProduct()
    {
        $this->form_validation->set_rules('pname', 'Product Item Name', 'required');
        $this->form_validation->set_rules('unitprice', 'Unit Price', 'required|numeric');
        $this->form_validation->set_rules('criamount', 'Critical Amount', 'required|numeric');
        $this->form_validation->set_rules('avaiamount', 'Available amount', 'required|numeric');


        if ($this->form_validation->run() == FALSE)
        {
            $this->Products();
        }else{
            $this->load->model('Model_productstock');
            $result= $this->Model_productstock->InsertNewProductItem();
            if($result==1){
                $this->session->set_flashdata('msgS','Product Item Successfully Inserted !!');
                redirect('ProductStockManage/Products');
            }elseif($result==2){
                $this->session->set_flashdata('msgW','This Product Item name all ready available !!');
                redirect('ProductStockManage/Products');
            }else{
                $this->session->set_flashdata('msgW','Something went wrong !!');
                redirect('ProductStockManage/Products');
            }
        }
    }

    //Manage data in products detail table
    Public function LoadProductDataTable()
    {
        $query = '';
        $this->load->model('Model_productstock');
        $this->load->library("pagination");
        $config = array();
        $config["base_url"] = "#";
        $config["total_rows"] = $this->Model_productstock->count_all();
        $config["per_page"] = 4;
        $config["uri_segment"] = 3;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li>';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li>';
        $config["last_tag_close"] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>Previous Page';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li>";
        $config["num_tag_close"] = "</li>";
        $config["num_links"] = 1;

        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $start = ($page - 1) * $config["per_page"];
        if ($this->input->get('querydata'))
        {
            $query = $this->input->get('querydata');
        }

        $output=array(
            'paginationdata' => $this->pagination->create_links(),
            'producttable'  => $this->Model_productstock->FetchingProductDataForTable($config["per_page"],$start, $query)
        );

        echo json_encode($output);
    }

    //Delete Product
    public function DeleteProduct()
    {
        $id = $this->input->get('productID');
        $this->load->model('Model_productstock');
        $result=$this->Model_productstock->DeleteProduct($id);
        $output=array('responce'=>$result);
        echo json_encode($output);
    }

    //Load Edit product ui
    public function EditProduct()
    {

        $this->GetCurrentUserData();
        $id = $this->input->get('id');
        $this->load->model('Model_productstock');
        //getting raw list

        $dataset=$this->Model_productstock->GetRequestedProductData($id);
        $this->data['productlist']=$dataset;
        $this->data['subview']='ProductStock/Products';
        $this->load->view('Home',$this->data);

    }

    //Edit product data
    public function UpdateProduct()
    {
        $pid = $this->input->get('id');
        $this->form_validation->set_rules('pname', 'Product Item Name', 'required');
        $this->form_validation->set_rules('unitprice', 'Unit Price', 'required|numeric');
        $this->form_validation->set_rules('criamount', 'Critical Amount', 'required|numeric');
        $this->form_validation->set_rules('avaiamount', 'Available amount', 'required|numeric');

        if ($this->form_validation->run() == FALSE)
        {
            redirect('ProductStockManage/EditProduct?id='.$pid);
        }else{
            $this->load->model('Model_productstock');
            $result= $this->Model_productstock->UpdateCurrentProduct($pid);
            if($result=='1') {
                $this->session->set_flashdata('msgS', 'Product Details Successfully Updated !!');
                redirect('ProductStockManage/Products');
            }else{
                $this->session->set_flashdata('msgW', 'Product name all ready available!!');
                redirect('ProductStockManage/Products');
            }
        }
    }



}