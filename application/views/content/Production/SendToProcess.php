<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Production Request Send To Process</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item  "><a href="ProductionRequest">Production
                    Requests</a></li>
            <li class="breadcrumb-item active">Send To Process</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10"><i
                    class="ti-settings text-white"></i></button>
    </div>
</div>
<?php foreach ($RequestOrder as $d) { ?>
<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div style="color: white;padding: 17px;padding-left: 31px;" class="card-body">
                <div class="row">
                    <div class="col-md-8"><h4 class="card-title">View Production Request Data</h4></div>
                    <div class="col-md-4"><input id="orderid" readonly class="form-control" style="color: white;"
                                                 value="<?php echo $d->oderID; ?>"></div>
                </div>


                <?php
                $this->load->model('Model_production');
                $product = $this->Model_production->DataRetrive('products', 'PID', $d->productid, 'productname');

                ?>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-10">
                                <label style="color: white" for="example-text-input" class=" col-form-label">
                                    Product Name</label>
                                <input readonly style=" color:white;" autocomplete="off" placeholder="Ex: 12"
                                       class="form-control" type="text" value="<?php echo $product; ?>">
                            </div>
                        </div>


                        <div class="form-group row">
                            <div class="col-10">
                                <label style="color: white" for="example-text-input" class=" col-form-label">
                                    Required Quantity</label>
                                <input readonly style=" color:white;" autocomplete="off" placeholder="Ex: 12"
                                       class="form-control" type="text" value="<?php echo $d->requestquantity; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-10">
                                <label style="color: white" for="example-text-input" class=" col-form-label">
                                    Ordered Date</label>
                                <input readonly style=" color:white;" autocomplete="off" placeholder="Ex: 12"
                                       class="form-control" type="text" value="<?php echo $d->insertdate; ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-10">
                                <label style="color: white" for="example-text-input" class=" col-form-label">
                                    Due Date</label>
                                <input readonly style=" color:white;" autocomplete="off" placeholder="Ex: 12"
                                       class="form-control" type="text" value="<?php echo $d->duedate; ?>">
                            </div>
                        </div>
                    </div>
                </div>

                <?php } ?>
            </div>
        </div>
    </div>

    <div class="col-md-5">
        <div class="card">
            <div style="color: white" class="card-body">
                <h4 class="card-title">Insert The Required Raw Materials</h4>



                <table>
                    <tbody>
                    <tr>
                        <th>Raw material</th>
                        <th>Amount</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>
                            <div class="form-group row">
                                <div style="    padding-right: 24%;">
                                    <select  style="color: white;" name="rawmaterials"
                                            class="form-control" id="sel">
                                        <?php
                                        foreach ($rawlist as $row) { ?>

                                            <ul>
                                                <li>
                                                    <option value="<?php echo $row->RID; ?>"><?php echo $row->Rname; ?></option>
                                                </li>
                                                ?>
                                            </ul>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </td>
                        <td>
                            <div class="form-group row">
                                <div class="col-10">
                                    <input  style=" color:white;" autocomplete="off"
                                           placeholder="Ex: 12"
                                           class="form-control" type="text" value="" name="requireamount">
                                </div>
                                <div id="updateamount"></div>
                                <?php echo form_error('updateamount', '<div style="margin-left: -2%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                            </div>
                        </td>
                        <td>
                            <button style="margin-top: -26px;" class="remove btn btn-danger" ><i class="fa fa-minus"></i></button>
                        </td>
                    </tr>
                    </tbody>
                </table>

                <div align="center">
                    <button  id="addRow" class="btn btn-success">
                        Add Row
                    </button>
                    <button  id="getValues" class="btn btn-primary">
                        Submit
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>

<script>


   var html='<tr><td> <div class="form-group row"> <div style="    padding-right: 24%;"> <select  style="color: white;" name="rawmaterials"class="form-control" id="sel"><?php foreach ($rawlist as $row) { ?><ul> <li> <option value="<?php echo $row->RID ?>"><?php echo $row->Rname ?></option> </li> ?> </ul><?php } ?></select> </div> </div> </td> <td> <div class="form-group row"> <div class="col-10"> <input id="changeamount" style=" color:white;" autocomplete="off"placeholder="Ex: 12"class="form-control" type="text" value="" name="requireamount"> </div> <div id="updateamount"></div><?php echo form_error('updateamount', '<div style="margin-left: -2%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?></div> </td> <td> <button style="margin-top: -26px;"class="remove btn btn-danger"><i class="fa fa-minus"></i></button> </td> </tr>'

    $(function () {

        $('#addRow').click(function () {
            $('tbody').append(html);
        });

        $(document).on('click', '.remove', function () {
            $(this).parents('tr').remove();
        });

        $('#getValues').click(function () {
            var values = [];
            var rawmaterials=[];
            var oid=$('#orderid').val();

            $('select[name="rawmaterials"]').each(function (i, elem) {
                rawmaterials.push($(elem).val());
            });

            $('input[name="requireamount"]').each(function (i, elem) {
                values.push($(elem).val());
            });

            $.ajax({
                url: "<?php echo base_url();?>index.php/ProductionManage/RawMaterialUsageForOrders",
                methode: "POST",
                dataType: "json",
                data: {"RawMaterials": rawmaterials,"RequireAmount":values,"OID":oid},
                success: function (data) {
                    if(data.msg=='sameraw')
                    {
                        swal({
                            title: "Oops!!",
                            type: "warning",
                            text: "Same Raw Material Choosed Twice",
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "close",
                            closeOnConfirm: true

                        });
                    }else if (data.msg=='notenough'){
                        swal({
                            title: "Oops!!",
                            type: "warning",
                            text: "Not Enough Raw matirials available for production",
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "close",
                            closeOnConfirm: true

                        });
                    }else{
                        swal({
                            title: "Success!!",
                            text: "Order Request send to processing state successfully!!",
                            type: "success"
                        }, function() {
                            window.location = "<?php base_url() ?>ProductionRequest";
                        });



                    }


                }, error: function (data) {
                    alert("Oops! something went Wrong");
                }
            });

        });
    });
</script>


