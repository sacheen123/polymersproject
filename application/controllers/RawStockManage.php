<?php

/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 06/11/2018
 * Time: 22:57
 */
class RawStockManage extends CI_Controller
{
    public $data=array('subview'=>'Oops subview not set','currentdata'=>'','categorylist'=>'','rawlist'=>'','mesurments'=>'');

    //Set current user data in topheader
    public function  GetCurrentUserData()
    {
        $this->load->model('Model_user');
        $result=$this->Model_user->GetUserData();
        $this->data['currentdata']=$result;

    }

//-----------------------------------------------------Raw materials section-------------------------------------------------------------


    //Load Raw materials manage ui
    public function RawItems()
    {
        $array[] = new stdClass;
        $array[0]->RID='';


        //getting category list
        $this->load->model('Model_rawstock');
        $categorylist=$this->Model_rawstock->FetchCategoryForDropdown();
        $this->data['categorylist']= $categorylist;

        //getting raw list
        $rawlist=$this->Model_rawstock->FetchRawMaterialsForDropdown();
        $this->data['rawlist']= $rawlist;

        $this->GetCurrentUserData();
        $this->data['currentdata']= $array;
        $this->data['subview']='RawStock/Raw';
        $this->load->view('Home',$this->data);
    }



    //Insert new Raw material
    public function InsertRawMaterial()
    {
        $this->form_validation->set_rules('rname', 'Raw Item Name', 'required');
        $this->form_validation->set_rules('unitprice', 'Category Name', 'required');
        $this->form_validation->set_rules('camount', 'Critical amount', 'required|numeric');

        if ($this->form_validation->run() == FALSE)
        {
            $this->RawItems();
        }else{
            $this->load->model('Model_rawstock');
            $result= $this->Model_rawstock->InsertNewRawItem();
            if($result==1){
                $this->session->set_flashdata('msgS','Raw Item Successfully Inserted !!');
                redirect('RawStockManage/RawItems');
            }elseif($result==2){
                $this->session->set_flashdata('msgW','This Raw Item name all ready available !!');
                redirect('RawStockManage/RawItems');
            }else{
                $this->session->set_flashdata('msgW','Something went wrong !!');
                redirect('RawStockManage/RawItems');
            }
        }
    }

    //Insert or remove  raw material availabale from the stock
    public function InsertRawmaterialamount()
    {
        $id = $this->input->get('action');
        if ($_POST['action'] == 'add') {

            $this->form_validation->set_rules('updateamount', 'Updating amount', 'required|numeric');

            if ($this->form_validation->run() == FALSE)
            {
                $this->RawItems();
            }else{
                $this->load->model('Model_rawstock');
                $result= $this->Model_rawstock->UpdateRawItemavailbleAmount("add");
                if($result==1){
                    $this->session->set_flashdata('msgS','Raw Item Current amount Successfully updated !!');
                    redirect('RawStockManage/RawItems');
                }elseif($result==2){
                    $this->session->set_flashdata('msgW','This Raw Item name Not available !!');
                    redirect('RawStockManage/RawItems');
                }else{
                    $this->session->set_flashdata('msgW','Something went wrong !!');
                    redirect('RawStockManage/RawItems');
                }
            }
        } else if ($_POST['action'] == 'remove') {
            $this->form_validation->set_rules('updateamount', 'Updating amount', 'required');

            if ($this->form_validation->run() == FALSE)
            {
                $this->RawItems();
            }else{
                $this->load->model('Model_rawstock');
                $result= $this->Model_rawstock->UpdateRawItemavailbleAmount("remove");
                if($result==1){
                    $this->session->set_flashdata('msgS','Raw Item Current amount Successfully updated !!');
                    redirect('RawStockManage/RawItems');
                }elseif($result==2){
                    $this->session->set_flashdata('msgW','This Raw Item name Not Available !!');
                    redirect('RawStockManage/RawItems');
                }elseif($result==3){
                    $this->session->set_flashdata('msgW','Something went wrong !!');
                    redirect('RawStockManage/RawItems');
                }elseif($result==5){
                    $this->session->set_flashdata('msgW','Entered amount Cant be deducted since low stock amount !!');
                    redirect('RawStockManage/RawItems');
                } else{
                    $this->session->set_flashdata('msgW','This Raw Material Out Of Stock !!');
                    redirect('RawStockManage/RawItems');
                }
            }
        }
    }

    //Manage data in raw materials table
    Public function LoadRawMaterialsDataTable()
    {
        $query = '';
        $this->load->model('Model_rawstock');
        $this->load->library("pagination");
        $config = array();
        $config["base_url"] = "#";
        $config["total_rows"] = $this->Model_rawstock->count_all_Rawmaterials();
        $config["per_page"] = 6;
        $config["uri_segment"] = 3;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li>';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li>';
        $config["last_tag_close"] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>Previous Page';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li>";
        $config["num_tag_close"] = "</li>";
        $config["num_links"] = 1;

        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $start = ($page - 1) * $config["per_page"];
        if ($this->input->get('querydata'))
        {
            $query = $this->input->get('querydata');
        }

        $output=array(
            'paginationdata' => $this->pagination->create_links(),
            'rawmaterialtable'  => $this->Model_rawstock->FetchingRawItemDataForTable($config["per_page"],$start, $query)
        );

        echo json_encode($output);
    }

    //Delete Raw material
    public function DeleteRawMaterial()
    {
        $id = $this->input->get('RawMaterialID');
        $this->load->model('Model_rawstock');
        $result=$this->Model_rawstock->DeleteRawMaterialData($id);
        $output=array('responce'=>$result);
        echo json_encode($output);
    }

    //load edit rawmaterial ui
    public function EditRawMaterial()
    {


        $id = $this->input->get('id');
        $this->load->model('Model_rawstock');
        //getting raw list
        $rawlist=$this->Model_rawstock->FetchRawMaterialsForDropdown();
        $this->data['rawlist']= $rawlist;

        $categorylist=$this->Model_rawstock->FetchCategoryForDropdown();
        $this->data['categorylist']= $categorylist;

        $dataset=$this->Model_rawstock->GetRequestedRawMatirialData($id);
        $this->data['currentdata']=$dataset;
        $this->data['subview']='RawStock/Raw';
        $this->load->view('Home',$this->data);

    }

    //Update raw matirials data
    public function UpdateRawMaterial()
    {
        $rid = $this->input->get('rid');

        $this->form_validation->set_rules('rname', 'Raw Item Name', 'required');
        $this->form_validation->set_rules('unitprice', 'Category Name', 'required');
        $this->form_validation->set_rules('camount', 'Critical amount', 'required|numeric');

        if ($this->form_validation->run() == FALSE)
        {
            redirect('RawStockManage/EditRawMaterial?id='.$rid);
        }else{
            $this->load->model('Model_rawstock');
            $result= $this->Model_rawstock->UpdateCurrentRawMaterial($rid);
            if($result=='1') {
                $this->session->set_flashdata('msgS', 'Raw material Successfully Updated !!');
                redirect('RawStockManage/RawItems');
            }else{
                $this->session->set_flashdata('msgW', 'Raw material Name Allready available !!');
                redirect('RawStockManage/RawItems');
            }
        }
    }


//-----------------------------------------------------category section-------------------------------------------------------------

    //Load RawStock Category manage UI
    public function Category()
    {
        $array[] = new stdClass;
        $array[0]->cid='';

        $this->load->model('Model_rawstock');
        $measurment=$this->Model_rawstock->RetriveAllMeasuremnts();
        $this->GetCurrentUserData();
        $this->data['mesurments']=$measurment;
        $this->data['subview']='RawStock/Category';
        $this->data['currentdata']= $array;
        $this->load->view('Home',$this->data);


    }

    //Load Edit category UI
    public function EditCategory()
    {
        $id = $this->input->get('id');
        $this->load->model('Model_rawstock');
        $measurment=$this->Model_rawstock->RetriveAllMeasuremnts();
        $dataset=$this->Model_rawstock->GetRequestedStockData($id);
        $this->data['currentdata']=$dataset;
        $this->data['mesurments']=$measurment;
        $this->data['subview']='RawStock/Category';
        $this->load->view('Home',$this->data);
    }

    //Insert new item category
    public function InsertCategory()
    {
        $this->form_validation->set_rules('cname', 'Category Name', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->Category();
        }else{
            $this->load->model('Model_rawstock');
            $result= $this->Model_rawstock->InsertNewCategory();
            if($result==1){
                $this->session->set_flashdata('msgS','Category Item Successfully Inserted !!');
                redirect('RawStockManage/Category');
            }elseif($result==2){
                $this->session->set_flashdata('msgW','This Category all ready available !!');
                redirect('RawStockManage/Category');
            }else{
                $this->session->set_flashdata('msgW','Something went wrong !!');
                redirect('RawStockManage/Category');
            }
        }
    }

    //update category details
    public function UpdateCategory()
    {
        $cid = $this->input->get('cid');

        $this->form_validation->set_rules('cname', 'Category Name', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            redirect('RawStockManage/EditCategory?id='.$cid);
        }else{
            $this->load->model('Model_rawstock');
            $result= $this->Model_rawstock->UpdateCurrentCategory($cid);
            if($result=='1') {
                $this->session->set_flashdata('msgS', 'Category Successfully Updated !!');
                redirect('RawStockManage/Category');
            }else{
                $this->session->set_flashdata('msgW', 'Category Name Already available !!');
                redirect('RawStockManage/Category');
            }
        }
    }

    //Manage data in category table
    Public function LoadCategoryDataTable()
    {
        $query = '';
        $this->load->model('Model_rawstock');
        $this->load->library("pagination");
        $config = array();
        $config["base_url"] = "#";
        $config["total_rows"] = $this->Model_rawstock->count_all();
        $config["per_page"] = 4;
        $config["uri_segment"] = 3;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li>';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li>';
        $config["last_tag_close"] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>Previous Page';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li>";
        $config["num_tag_close"] = "</li>";
        $config["num_links"] = 1;

        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $start = ($page - 1) * $config["per_page"];
        if ($this->input->get('querydata'))
        {
            $query = $this->input->get('querydata');
        }

        $output=array(
            'paginationdata' => $this->pagination->create_links(),
            'categorytable'  => $this->Model_rawstock->FetchingCategoryDataForTable($config["per_page"],$start, $query)
        );

        echo json_encode($output);
    }


    //Deleting category
    public function DeleteCategory()
    {
        $id = $this->input->get('categoryID');
        $this->load->model('Model_rawstock');
        $result=$this->Model_rawstock->DeleteCategory($id);
        $output=array('responce'=>$result);
        echo json_encode($output);
    }


    //---------------------------------------------Report export---------------------------------------------------------------
    //Export Excel
    public function ExportAsExcel()
    {
        $this->load->model('Model_rawstock');
        $rawmaterialdata=$this->Model_rawstock->FetchforexcelRawMaterials();
        $this->load->library("excel");
        $object=new PHPExcel();
        $object->setActiveSheetIndex(0);
        $table_columns=array("Raw Material","category","unitprice(RS:)","Critical Amount","available Amount","createDate");
        $column=0;
        foreach ($table_columns as $field)
        {
            $object->getActiveSheet()->setCellValueByColumnAndRow($column,1,$field);
            $column++;
        }

        $excel_row=2;
        foreach ($rawmaterialdata as $row)
        {
            $catgory=$this->Model_rawstock->DataRetrive('categories','cid',$row->Rcategoryid,'cname');
            $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->Rname);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$catgory);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$row->oneprice);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$row->criticalamount);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$row->currentamount);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5,$excel_row,$row->insertdate);
            $excel_row++;
        }

        $object_writer=PHPExcel_IOFactory::createWriter($object,'Excel5');
        header('Content-Type:application/vnd.ms-excel');
        header('Content-Disposition:attachment;filename="RawStock-Data.xls"');
        $object_writer->save('php://output');
    }

    //choosed item available amount send to front end
    public function ChoosedItemAvailableAmount()
    {
        $id = $this->input->get('chooseid');
        $this->load->model('Model_rawstock');
        $rawmaterialdata=$this->Model_rawstock->DataRetrive('rawmaterials','RID',$id,'currentamount');
        $releventcid=$this->Model_rawstock->DataRetrive('rawmaterials','RID',$id,'Rcategoryid');
        $relevantmtype=$this->Model_rawstock->DataRetrive('categories','cid',$releventcid,'mtype');
        $relevantmeasurment=$this->Model_rawstock->DataRetrive('measurements','mid',$relevantmtype,'mesurement');
        $output=array(
            'cuurentamount' => $rawmaterialdata,
            'measurement'=>$relevantmeasurment

        );

        echo json_encode($output);
    }





//-----------------------------------------------------Measurment----------------------------------------------------------------

    public function InsertMeasurment()
    {
        $this->form_validation->set_rules('mesname', 'Measurment Name', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->Category();
        }else{
            $this->load->model('Model_rawstock');
            $result= $this->Model_rawstock->InsertNewMeasurement();
            if($result==1){
                $this->session->set_flashdata('msgS','Measurement Successfully Inserted !!');
                redirect('RawStockManage/Category');
            }elseif($result==2){
                $this->session->set_flashdata('msgW','Measurement all ready available !!');
                redirect('RawStockManage/Category');
            }else{
                $this->session->set_flashdata('msgW','Measurement went wrong !!');
                redirect('RawStockManage/Category');
            }
        }
    }

 //--------------------------------------------------Notification genaration ssection for raw materials-----------------------------------------------

    //Inserting Notification to notification table
   public function CriticalNotificationRawMat()
   {
       $RawMatId = $this->input->get('chooseid');
       $ChangeAmount = $this->input->get('changeAmount');
       $this->load->model("Model_rawstock");
       $result=$this->Model_rawstock->CriticalNotificationCheck($RawMatId,$ChangeAmount);
       if($result==1)
       {
           $output=array(
               'status' => 'true'
           );

           echo json_encode($output);
       }else{
           $output=array(
               'status' => 'false'
           );

           echo json_encode($output);
       }
   }

   //Fetch All notifications form notification table
    public function FetchAllNotifications()
    {
        if (isset($_GET['view'])) {
            {
                $this->load->model("Model_rawstock");
                if ($_GET['view']!='')
                {
                    $this->Model_rawstock->SetReadNotifications();
                }
                $result = $this->Model_rawstock->FetchNotifications();
                $unreadcount = $this->Model_rawstock->CountUnreadMsg();
                $output = array(
                    'DataList' => $result,
                    'UnreadCount' => $unreadcount
                );
                echo json_encode($output);
            }

        }
    }


}

