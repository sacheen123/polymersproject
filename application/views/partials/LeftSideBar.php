

<!-- ============================================================== -->
    <!-- Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->
    <aside class="left-sidebar">
        <!-- Sidebar scroll-->
        <div class="scroll-sidebar">
            <!-- User profile -->
            <div class="user-profile">
                <!-- User profile image -->
                <div class="profile-img"> <img src="<?php echo base_url()?>/assets/images/users/5.jpg" alt="user" /> </div>
                <!-- User profile text-->
                <div class="profile-text"> <a href="#" class="dropdown-toggle link u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><?php echo $this->session->userdata('username'); ?>  <span class="caret"></span></a>
                    <div class="dropdown-menu animated flipInY">
                    </div>
                </div>
            </div>
            <!-- End User profile text-->
            <!-- Sidebar navigation-->
            <?php if ($this->session->userdata('type')=="Admin"){ ?>
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-small-cap">PERSONAL</li>

                        <li>
                            <a  href="http://localhost/App/" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Dashboard </span></a>
                        </li>
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-user-o"></i><span class="hide-menu"> User Managment </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href='<?php echo base_url('index.php/UsersManage/AddUser')?>'>Add Users</a></li>
                                <li><a href='<?php echo base_url('index.php/UsersManage/ViewUser')?>'>View Users</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-database"></i><span class="hide-menu"> Raw Materials Stock  </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href='<?php echo base_url('index.php/RawStockManage/Category')?>'>Category Manage</a></li>
                                <li><a href='<?php echo base_url('index.php/RawStockManage/RawItems')?>'>Raw Materials Manage</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-cubes"></i><span class="hide-menu"> Products Stock</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href='<?php echo base_url('index.php/ProductStockManage/Products')?>'>Products Manage</a></li>
                            </ul>
                        </li>
                        <li>
                            <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-shopping-basket"></i><span class="hide-menu"> Order Managment </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php echo base_url()?>index.php/OrderManage/AddOrder">Add Orders</a></li>
                                <li><a href="<?php echo base_url()?>index.php/OrderManage/ViewOrder">View Orders</a></li>
                            </ul>
                        </li>
                        <li>
                                <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-gear"></i><span class="hide-menu">Production Managment </span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php echo base_url()?>index.php/ProductionManage/ProductionRequest">Requests List</a></li>
                                <li><a href="<?php echo base_url()?>index.php/ProductionManage/ProductionProcessing">Processing List</a></li>
                                <li><a href="<?php echo base_url()?>index.php/ProductionManage/ProductionDone">Done List</a></li>
                            </ul>
                        </li>
<!--                        <li>-->
<!--                            <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-database"></i><span class="hide-menu"> Reports</span></a>-->
<!--                        </li>-->
                    </ul>
                </nav>
            <?php }else if($this->session->userdata('type')=="StockManager"){ ?>
            <nav class="sidebar-nav">
                <ul id="sidebarnav">
                    <li>
                        <a class="has-arrow" href="#" aria-expanded="false"><i class="fa fa-database"></i><span class="hide-menu"> Raw Materials Stock  </span></a>
                        <ul aria-expanded="false" class="collapse">
                            <li><a href='<?php echo base_url('index.php/RawStockManage/Category')?>'>Category Manage</a></li>
                            <li><a href='<?php echo base_url('index.php/RawStockManage/RawItems')?>'>Raw Materials Manage</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>

            <?php } ?>


            <!-- End Sidebar navigation -->


        </div>
        <!-- End Sidebar scroll-->
        <!-- Bottom points-->
        <div class="sidebar-footer">
            <!-- item-->
            <div style="margin-left: 54px;" >
                <a href="<?php echo base_url('index.php/Login/Logout')?>" class="link" data-toggle="tooltip" title="Logout">
                    <div class="row">
                        <div class="col-d-4"></div> <i class="mdi mdi-power"></i></div>
                        <div style="margin-top: -28px;margin-left: 5px;" class="col-d-8">Logout</div>

                </a>
            </div>
        </div>
        <!-- End Bottom points-->
    </aside>
    <!-- ============================================================== -->
    <!-- End Left Sidebar - style you can find in sidebar.scss  -->
    <!-- ============================================================== -->