<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Raw Material Stock Managment</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Raw Materials Manage</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10"><i
                    class="ti-settings text-white"></i></button>
    </div>
</div>

<div class="row">
    <div class="col-md-7">
        <div class="card">
            <div style="color: white;padding: 17px" class="card-body">
                <h4 class="card-title">View Categories</h4>
                <div class="table-responsive m-t-40">
                    <div id="example23_wrapper" class="dataTables_wrapper">
                        <div class="dt-buttons">
                            <form method="get" action="<?php base_url(); ?>ExportAsExcel">
                                <input style="border-color: transparent; cursor: pointer;" value="Excel" type="submit"
                                       name="export" class="dt-button buttons-excel buttons-html5" tabindex="0"
                                       aria-controls="example23">
                            </form>
                        </div>
                        <div id="example23_filter" class="dataTables_filter">
                            <label>Search:<input placeholder="Raw Material Name" style="color: white;" id="search_text" type="search" class=""  aria-controls="example23"></label>
                        </div>
                        <div id="RawMaterial_table" class="dataTables_wrapper"></div>
                        <div align="right" id="pagination_link"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-5">
        <div class="card">
            <div style="color: white" class="card-body">
                <h4 class="card-title">Update the Current Amount</h4>

                <?php echo form_open('RawStockManage/InsertRawmaterialamount'); ?>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <div >
                                <label style="color: white" for="example-email-input"
                                       class=" col-form-label">Raw material</label>

                                <select  onchange='DisplayCurrentAmount()'  style="color: white;" name="rawtype" class="form-control" id="sel">

                                    <?php
                                    foreach ($rawlist as $row) {  ?>

                                        <ul>
                                       <li><option   value="<?php echo $row->RID ?>"><?php echo $row->Rname ?></option></li> ?>
                                        </ul>
                                  <?php  }?>

                                </select>

                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-10">
                                <label style="color: white" for="example-text-input" class=" col-form-label">
                                    Amount (<span id="messpan"></span>)</label>
                                <input id="changeamount" style=" color:white;" autocomplete="off" placeholder="Ex: 12"
                                       class="form-control" type="text" value="" name="updateamount">
                            </div>
                            <div id="updateamount"></div>
                            <?php echo form_error('updateamount', '<div style="margin-left: -2%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">

                        <button name="action" value="add" data-toggle="tooltip" data-original-title="Insert" id="myBtnb" style="padding-left: 10%;padding-right: 10%;" type="submit"
                                class="btn btn-success"><i class="fa fa-plus"></i>
                        </button>
                        <button onclick="NotificationsRawMat()" name="action" value="remove" data-toggle="tooltip" data-original-title="Remove" id="myBtnb" style="padding-left: 10%;padding-right: 10%;" type="submit"
                                class="btn btn-danger"><i class="fa fa-minus"></i>
                        </button>
                    </div>
                    <div class="col-md-8">
                        <div style="color:black;background-color:white;margin-left: -28px;width: 108%;padding-top: 2px; padding-bottom: 2px; margin-top: 5px;" class="alert alert-danger alert-rounded">Available Amount:  <strong id="amount"></strong><strong id="mes"></strong> </div>
                    </div>

                    <div id="wrongmsg"></div>

                </div>
                <?php echo form_close(); ?>
            </div>
        </div>

        <?php foreach ($currentdata as $d)
        {
            if($d->RID!=''){ ?>
                <div class="card">
                    <div style="color: white" class="card-body">
                        <h4 class="card-title">Insert Raw Material</h4>

                        <?php echo form_open('RawStockManage/UpdateRawMaterial?rid='.$d->RID); ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-10">
                                        <label style="color: white" for="example-text-input" class=" col-form-label">Raw
                                            Material</label>
                                        <input style=" color:white;" autocomplete="off" placeholder="Ex: CPL Blank"
                                               style="color: white" class="form-control" type="text" value="<?php echo $d->Rname ?>" name="rname">
                                    </div>
                                    <div id="cnamecontent"></div>
                                    <?php echo form_error('rname', '<div style="margin-left: -2%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                                </div>
                                <div class="form-group row">
                                    <div class="col-10">
                                        <label style="color: white" for="example-email-input"
                                               class=" col-form-label">Category</label>
                                        <select style="color: white;" name="categorytype" class="form-control" id="sel1">
                                            <?php
                                            foreach ($categorylist as $row) { ?>
                                             <option  <?= $d->Rcategoryid ==  $row->cid ? ' selected="selected"' : '';?>  value="<?php echo $row->cid;?>"><?php echo $row->cname; ?></option>';
                                           <?php }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-10">
                                        <label style="color: white" for="example-text-input" class=" col-form-label">Critical Amount</label>
                                        <input style=" text-transform: uppercase;color:white;" autocomplete="off" placeholder="Ex: 100" style="color: white" class="form-control" type="text" value="<?php echo $d->criticalamount; ?>" name="camount">
                                    </div>
                                    <div id="cnamecontent"></div>
                                    <?php echo form_error('camount', '<div style="margin-left: 3%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                                </div>
                                <div class="form-group row">
                                    <div class="col-10">
                                        <label style="color: white" for="example-text-input" class=" col-form-label">Price of 1 unit (Rs:)</label>
                                        <input style=" text-transform: uppercase;color:white;" autocomplete="off"  placeholder="Ex:100.50" style="color: white" class="form-control" type="text"
                                               value="<?php echo $d->oneprice ?>" name="unitprice">
                                    </div>
                                    <div id="cnamecontent"></div>
                                    <?php echo form_error('unitprice', '<div style="margin-left: -2%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div>

                            <button id="myBtna" style="padding-left: 3%;padding-right: 3%;" type="submit"
                                    class="btn btn-success">Update
                            </button>
                            <a href="<?php echo base_url()?>index.php/RawStockManage/RawItems" id="myBtna" role="button" style="padding-left: 3%;padding-right: 3%;" type="submit"
                                    class="btn btn-danger">Back
                            </a>
                            <div id="wrongmsg"></div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>


       <?php     }else{    ?>

                <div class="card">
                    <div style="color: white" class="card-body">
                        <h4 class="card-title">Insert Raw Material</h4>

                        <?php echo form_open('RawStockManage/InsertRawMaterial'); ?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-10">
                                        <label style="color: white" for="example-text-input" class=" col-form-label">Raw
                                            Material</label>
                                        <input style=" color:white;" autocomplete="off" placeholder="Ex: CPL Blank"
                                               style="color: white" class="form-control" type="text" value="" name="rname">
                                    </div>
                                    <div id="cnamecontent"></div>
                                    <?php echo form_error('rname', '<div style="margin-left: -2%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                                </div>
                                <div class="form-group row">
                                    <div class="col-10">
                                        <label style="color: white" for="example-email-input"
                                               class=" col-form-label">Category</label>
                                        <select style="color: white;" name="categorytype" class="form-control" id="sel1">
                                            <?php
                                            foreach ($categorylist as $row) {
                                                echo ' <option value="' . $row->cid . '">' . $row->cname . '</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-10">
                                        <label style="color: white" for="example-text-input" class=" col-form-label">Critical Amount</label>
                                        <input style=" text-transform: uppercase;color:white;" autocomplete="off" placeholder="Ex: 100" style="color: white" class="form-control" type="text" value="" name="camount">
                                    </div>
                                    <div id="cnamecontent"></div>
                                    <?php echo form_error('camount', '<div style="margin-left: 3%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                                </div>
                                <div class="form-group row">
                                    <div class="col-10">
                                        <label style="color: white" for="example-text-input" class=" col-form-label">Price of 1 unit (Rs:)</label>
                                        <input style=" text-transform: uppercase;color:white;" autocomplete="off"
                                               placeholder="Ex:100.50" style="color: white" class="form-control" type="text"
                                               value="" name="unitprice">
                                    </div>
                                    <div id="cnamecontent"></div>
                                    <?php echo form_error('unitprice', '<div style="margin-left: -2%;margin-top: 0.3%; " class=" tst4 btn btn-danger">', '</div>'); ?>
                                </div>
                            </div>
                        </div>
                        <div>

                            <button id="myBtna" style="padding-left: 3%;padding-right: 3%;" type="submit"
                                    class="btn btn-success">Insert
                            </button>
                            <div id="wrongmsg"></div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>


        <?php    }
        }
            ?>

    </div>
</div>

<script>

    //Genarating notification for Raw materials amount
    function NotificationsRawMat() {

        var RawMatId=sel.options[sel.selectedIndex].value;  //changing raw material ID
        var ChangingAmount=document.getElementById("changeamount").value;  //changing raw material amount

        $(document).ready(function () {

                $.ajax({
                    url: "<?php echo base_url();?>index.php/RawStockManage/CriticalNotificationRawMat" ,
                    methode: "POST",
                    dataType: "json",
                    data: {"chooseid": RawMatId,"changeAmount":ChangingAmount},
                    success: function (data) {
                        $('#testdata').html(data.status);

                    }, error: function (data) {
                        console.log(data);
                        alert("Oops! something went Wrong");
                    }
                });

        });

    }

    //Displaying current available raw material amount in inert or remove raw material form
    DisplayCurrentAmount();
    function  DisplayCurrentAmount() {
        var e = document.getElementById("sel");

        var choosedid = e.options[e.selectedIndex].value;
        $(document).ready(function () {
            $.ajax({
                url: "<?php echo base_url();?>index.php/RawStockManage/ChoosedItemAvailableAmount" ,
                methode: "POST",
                dataType: "json",
                data: {"chooseid": choosedid},
                success: function (data) {
                    console.log(data)
                     $('#amount').html(data.cuurentamount);
                     $('#mes').html(data.measurement);
                    $('#messpan').html(data.measurement);
                }, error: function (data) {
                    console.log(data);
                    alert("Oops! something went Wrong");
                }
            });
        });

    }

    //Delete Raw Matirials
    function DeleteRawData(id) {

        $(document).ready(function () {
            var cid = id;
            swal({
                title: "Are you sure?",
                text: "Do yo want to Delete this Raw Material!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                $.ajax({
                    url: "<?php echo base_url();?>index.php/RawStockManage/DeleteRawMaterial",
                    methode: "POST",
                    dataType: "json",
                    data: {"RawMaterialID": cid},
                    success: function (data) {
                        swal("Deleted!", "Selected Raw Material deleted.", "success");
                        LoadRawMaterialsTable(pagevalue, '');

                    }, error: function (data) {
                        alert("Oops! something went Wrong");
                    }
                });


            });
        });

    }

    var pagevalue = 1;

    //load category data table function
    window.LoadRawMaterialsTable = function (page, searchdata) {
        $.ajax({
            url: "<?php echo base_url();?>index.php/RawStockManage/LoadRawMaterialsDataTable/" + page,
            methode: "POST",
            dataType: "json",
            data: {"querydata": searchdata},
            success: function (data) {
                console.log(data)
                $('#RawMaterial_table').html(data.rawmaterialtable);
                $('#pagination_link').html(data.paginationdata);
            }, error: function (data) {
                console.log(data);
                alert("Oops! something went Wrong");
            }
        });
    }


    $(document).ready(function () {
        //calling load data to table function
        LoadRawMaterialsTable(pagevalue, '');

        //pagination click events
        $(document).on("click", ".pagination li a", function (event) {
            event.preventDefault();
            pagevalue = $(this).data("ci-pagination-page");
            var search = document.getElementById("search_text").value;
            console.log(search);
            LoadRawMaterialsTable(pagevalue, search);
        });

        //search input click events
        $('#search_text').keyup(function () {
            var search = $(this).val();
            if (search != '') {
                LoadRawMaterialsTable(pagevalue, search);
            }
            else {
                LoadRawMaterialsTable(pagevalue, '');
            }
        });


    });
</script>