<script src="<?php echo base_url()?>/assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="<?php echo base_url()?>/assets/plugins/bootstrap/js/popper.min.js"></script>
<script src="<?php echo base_url()?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="<?php echo base_url()?>/assets/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url()?>/assets/js/waves.js"></script>
<!--Menu sidebar -->
<script src="<?php echo base_url()?>/assets/js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="<?php echo base_url()?>/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<!--Custom JavaScript -->
<script src="<?php echo base_url()?>/assets/js/custom.min.js"></script>
<!-- Sweet-Alert  -->
<script src="<?php echo base_url()?>/assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="<?php echo base_url()?>/assets/plugins/sweetalert/jquery.sweet-alert.custom.js"></script>
<script src="<?php echo base_url()?>/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- ============================================================== -->
<!-- This page plugins -->
<!-- ============================================================== -->
<!-- chartist chart -->
<script src="<?php echo base_url()?>/assets/plugins/chartist-js/dist/chartist.min.js"></script>
<script src="<?php echo base_url()?>/assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
<!-- Chart JS -->
<script src="<?php echo base_url()?>/assets/plugins/echarts/echarts-all.js"></script>
<!-- Chart JS -->
<script src="<?php echo base_url()?>/assets/js/dashboard1.js"></script>
<!-- ============================================================== -->
<!-- Style switcher -->
<!-- ============================================================== -->
<script src="<?php echo base_url()?>/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>