<div class="row page-titles">
    <div class="col-md-6 col-8 align-self-center">
        <h3 class="text-themecolor m-b-0 m-t-0">Order Managment</h3>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Add Order</li>
        </ol>
    </div>
    <div class="col-md-6 col-4 align-self-center">
        <button class="right-side-toggle waves-effect waves-light btn-info btn-circle btn-sm pull-right m-l-10"><i
                    class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<div class="row">
    <div class="col-lg-12 col-md-12">

        <h4 class="card-title">Add New Order</h4>
        <br/>
        <?php $dataset = array('onsubmit' => 'return validateOredrForm()', 'name' => 'OredrAddForm'); ?>
        <?php echo form_open('OrderManage/InsertOrder', $dataset); ?>
        <div style="border: 1px white solid;padding: 45px;">
            <div style="color:white;border: 2px white" class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">

                    <!--Order ID field-->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label style="margin-top: 6%;"> Order ID</label>
                            </div>
                            <div class="col-md-9">
                                <input readonly name="orderid" id="orderid" style="color: white;" value="" type="text"
                                       class="form-control mydatepicker">
                            </div>
                        </div>
                    </div>

                    <!--Oreder Date field-->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label style="margin-top: 6%;"> Order Date</label>
                            </div>
                            <div class="col-md-9">
                                <input name="orderdate" id="orderdate" style="color: white;" type="date"
                                       class="form-control mydatepicker">
                            </div>
                        </div>
                    </div>

                    <!--Delivary date field-->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label style="margin-top: 6%;"> Delivary Date</label>
                            </div>
                            <div class="col-md-9">
                                <input name="delivarydate" id="delivarydate" style="color: white;" value="" type="date"
                                       class="form-control mydatepicker">
                            </div>
                        </div>
                    </div>

                    <!--Customer name field-->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label style="  "> Customer Name</label>
                            </div>
                            <div class="col-md-9">
                                <input name="cusname" style="color: white;" type="text"
                                       class="form-control mydatepicker" placeholder="Ex: John Smith">
                            </div>
                        </div>
                        <div style="margin-left: 27%;" id="cusnamecontent"></div>
                        <?php echo form_error('cusname', '<div style="margin-left: 27%;" class=" tst4 btn btn-danger">', '</div>'); ?>
                    </div>

                    <!--Customer address field-->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label style="  "> Customer Address</label>
                            </div>
                            <div class="col-md-9">
                                <textarea name="cusaddress" style="color: white;" class="form-control" rows="5"
                                          placeholder="Ex: No 05,Malabe,Colombo"></textarea>
                            </div>
                        </div>
                        <div style="margin-left: 27%;" id="cusaddresscontent"></div>
                        <?php echo form_error('cusaddress', '<div style="margin-left: 27%;" class=" tst4 btn btn-danger">', '</div>'); ?>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>

            <div class="card">
                <div style="color: white" class="card-body">
                    <h2 align="center" class="card-title">Make Order List</h2>
                    <table class="table color-table inverse-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Item</th>
                            <th>Unit Price</th>
                            <th>Available amount</th>
                            <th>Quantity</th>
                            <th>Total Price</th>

                        </tr>
                        </thead>
                        <div>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    <div style=" width: 160px;">
                                        <select onchange='FillItemFields();   pricecalculate();' style="color: white;"
                                                name="itemname" class="form-control" id="itemtype">

                                            <?php
                                            foreach ($itemList as $row) { ?>

                                                <ul>
                                                    <li>
                                                        <option value="<?php echo $row->PID ?>"><?php echo $row->productname ?></option>
                                                    </li>
                                                    ?>
                                                </ul>
                                            <?php } ?>

                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <input id="unitprice" name="unitprice" readonly style="color: white;" type="text" class="form-control mydatepicker">
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <input id="availableamount" name="availableamount" readonly style="color: white;" type="text" class="form-control mydatepicker">
                                    </div>
                                </td>
                                <td>
                                    <div>
                                        <input id="quantitiy" name="quantity" onkeyup="pricecalculate()" style="color: white;" type="text" class="form-control mydatepicker" placeholder="Ex: 4">
                                    </div>
                                    <div style="margin-left: 27%;" id="quantitycontent"></div>
                                </td>
                                <td>
                                    <div>
                                        <input name="itemtotalprice" id="tot" readonly style="color: white;" type="text"
                                               class="form-control mydatepicker">
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </div>

                    </table>
<!--                    <div align="center">-->
<!--                        <button type="button" class="btn btn-success">Add Row</button>-->
<!--                        <button type="button" class="btn btn-danger">Remove Row</button>-->
<!--                    </div>-->

                </div>
            </div>

            <div style="color:white;border: 2px white" class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">

                    <!--sub total field-->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label style="margin-top: 6%;"> Sub Total</label>
                            </div>
                            <div class="col-md-9">
                                <input style="color: white" name="subtot" class="form-control mydatepicker" readonly id="subtot"
                                       value="" type="text">
                            </div>
                        </div>
                    </div>

                    <!--Tax field-->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label style="  "> Tax(5%)</label>
                            </div>
                            <div class="col-md-9">
                                <input name="tax" style="color: white" readonly id="tax" type="text"  class="form-control mydatepicker">
                            </div>
                        </div>
                    </div>

                    <!--Net total field-->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label style="  "> Net total</label>
                            </div>
                            <div class="col-md-9">
                                <input name="nettotal" id="nettot" style="color: white;" readonly style="color: white;" class="form-control" rows="5" >
                            </div>
                        </div>
                    </div>

                    <!--discount field-->
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-3">
                                <label style="  "> Discount(Rs:)</label>
                            </div>
                            <div class="col-md-9">
                                <input name="discount" onkeydown="DiscountCal()" onkeyup="DiscountCal()" style="color: white"  id="discount" class="form-control" rows="5" placeholder="Ex: 1200">
                            </div>
                            <div style="margin-left: 27%;" id="discountcontent"></div>
                        </div>
                    </div>

                </div>
                <div class="col-md-3"></div>
            </div>


            <div class="card">
                <div style="color: white" class="card-body">
                    <h2 align="center" class="card-title">Payment details</h2><br/>
                    <div class="row">
                        <div class="col-md-4">
                            <!--Customer address field-->
                            <div class="form-group">
                                <div style="padding-left: 20px; padding-right: 14px;" class="row">
                                    <label style="  "> Payment Type</label>
                                    <select name="paymenttype" style="color: white;background-color: #272c33;" class="custom-select col-12" id="inlineFormCustomSelect">
                                        <option value="Cash">Cash</option>
                                        <option value="Cheque">Cheque</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!--Customer address field-->
                            <div class="form-group">
                                <div style="padding-left: 8px; padding-right: 14px;" class="row">
                                    <label style="  "> Paid Amount</label>
                                    <input name="paidamount" id="paidamount" style="color: white;" class="form-control" rows="5" placeholder="Ex:150">
                                </div>
                                <div style="margin-left: 27%;" id="paidamountcontent"></div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!--Customer address field-->
                            <div class="form-group">
                                <div style="padding-left: 8px; padding-right: 26px;" class="row">
                                    <label style="  "> Due Amount</label>
                                    <input name="dueamount" id="dueamount" style="color: white;" class="form-control" rows="5" readonly>
                                </div>
                                <div style="margin-left: 27%;" id="dueamountcontent"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <button id="orderbtn" style="padding-left: 12%; padding-right: 12%;" type="submit"
                            class="btn btn-success">Order
                    </button>
                </div>
                <div class="col-md-4"></div>
            </div>
            <div id="wrongmsg"></div>
        </div>
        <?php echo form_close(); ?>
    </div>


    <script>

        var newprice;

        //set current date for order date as default
        Date.prototype.toDateInputValue = (function () {
            var local = new Date(this);
            local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
            return local.toJSON().slice(0, 10);
        });

        document.getElementById('orderdate').value = new Date().toDateInputValue();


        //set order id
        $(document).ready(function () {
            $.ajax({
                url: "<?php echo base_url();?>index.php/OrderManage/GenarateOrderID",
                methode: "POST",
                dataType: "json",
                success: function (data) {
                    console.log(data)
                    $('#orderid').val(data.orderid);
                }, error: function (data) {
                    console.log(data);
                    alert("Oops! something went Wrong in genarating order id");
                }
            });
        });

        //Set item details according to the choosed item

        FillItemFields();

        function FillItemFields() {
            var e = document.getElementById("itemtype");

            var choosedid = e.options[e.selectedIndex].value;
            $(document).ready(function () {
                $.ajax({
                    url: "<?php echo base_url();?>index.php/OrderManage/GetItemDataForFields",
                    methode: "POST",
                    dataType: "json",
                    data: {"chooseid": choosedid},
                    success: function (data) {
                        console.log(data)
                        $('#unitprice').val(data.unitprice);
                        $('#availableamount').val(data.availableamount);
                    }, error: function (data) {
                        console.log(data);
                        alert("Oops! something went Wrong in item details feilds loadig methode");
                    }
                });
            });

        }

        //calculating total price for selected item
        function pricecalculate() {
            var quan = document.getElementById("quantitiy").value;

            $(document).ready(function () {
                var state = $.isNumeric(quan);

                if ($('#quantitiy').val() == '') {
                    $('#tot').val('');
                    $('#subtot').val('');
                    $('#tax').val('');

                    $('#nettot').val('');
                    document.getElementById("quantitycontent").innerHTML = '<div style="margin-left: -33%; margin-top: 3%;" class=" tst4 btn btn-danger">This Feild Required</div>';
                    $('#discount').val()
                    document.getElementsById("discount").readOnly = true;
                    document.getElementById("orderbtn").disabled = true;
                    document.getElementById("wrongmsg").innerHTML = '<strong><h5 style="  margin-left: 48%;margin-top: -3%;color: red;">Correct the Invalid Fields</h5></strong>';
                } else {

                    if (state == false) {
                        document.getElementById("quantitycontent").innerHTML = '<div style="margin-left: -33%; margin-top: 3%;" class=" tst4 btn btn-danger">Enter Numers Only</div>';
                        document.getElementById("orderbtn").disabled = true;
                        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style="  margin-left: 48%;margin-top: -3%;color: red;">Correct the Invalid Fields</h5></strong>';
                    } else {
                        if (quan % 1 != 0) {
                            document.getElementById("quantitycontent").innerHTML = '<div style="margin-left: -33%; margin-top: 3%;" class=" tst4 btn btn-danger">Please Enter valid number</div>';
                            document.getElementById("orderbtn").disabled = true;
                            document.getElementById("wrongmsg").innerHTML = '<strong><h5 style="  margin-left: 48%;margin-top: -3%;color: red;">Correct the Invalid Fields</h5></strong>';
                        } else {
                            document.getElementById("quantitycontent").innerHTML = '';
                            document.getElementById("orderbtn").disabled = false;
                            document.getElementById("wrongmsg").innerHTML = '';

                            var unitprice = $('#unitprice').val()
                            var tot = unitprice * quan;
                            var tax = (tot / 100) * 5;
                            newprice = tot + tax;
                            $('#tot').val(tot);
                            $('#subtot').val(tot);
                            $('#tax').val(tax);

                            calculatenettot(tot, tax, 0);

                        }
                    }
                }

            })
        }

        function calculatenettot(tot, tax, dis) {
            var lastprice = (tot + tax) - dis;

            $(document).ready(function () {

                $('#nettot').val(lastprice);
                $('#dueamount').val(lastprice);
            })

        }


        //calculating discount
        function DiscountCal() {
            $(document).ready(function () {
                if ($('#discount').val() != '') {
                    var discnt = $('#discount').val();
                    var disstate = $.isNumeric(discnt);

                    if (disstate == false) {
                        document.getElementById("discountcontent").innerHTML = '<div style="margin-left: 3%; margin-top: 3%;" class=" tst4 btn btn-danger">Enter Numers Only</div>';
                        document.getElementById("orderbtn").disabled = true;
                        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style="  margin-left: 48%;margin-top: -3%;color: red;">Correct the Invalid Fields</h5></strong>';
                    } else {
                        document.getElementById("discountcontent").innerHTML = '';
                        document.getElementById("orderbtn").disabled = false;
                        document.getElementById("wrongmsg").innerHTML = '';

                        var subtot = $('#subtot').val();
                        var tax = $('#tax').val();
                        if (subtot == '' && discnt != '') {
                            document.getElementById("discountcontent").innerHTML = '<div style="margin-left: 3%; margin-top: 3%;" class=" tst4 btn btn-danger">Please Enter quantity first</div>';
                            document.getElementById("wrongmsg").innerHTML = '<strong><h5 style="  margin-left: 48%;margin-top: -3%;color: red;">Correct the Invalid Fields</h5></strong>';
                        } else {
                            document.getElementById("discountcontent").innerHTML = '';
                            document.getElementById("wrongmsg").innerHTML = '';
                            if ($('#paidamount').val() != '') {
                                var paidamount = $('#paidamount').val();
                                var paidstate = $.isNumeric(paidamount);
                                if (paidstate == false) {
                                    document.getElementById("paidamountcontent").innerHTML = '<div style="margin-left: 3%; margin-top: 3%;" class=" tst4 btn btn-danger">Enter Numers Only</div>';
                                    document.getElementById("orderbtn").disabled = true;
                                    document.getElementById("wrongmsg").innerHTML = '<strong><h5 style="  margin-left: 48%;margin-top: -3%;color: red;">Correct the Invalid Fields</h5></strong>';
                                } else {
                                    document.getElementById("paidamountcontent").innerHTML = '';
                                    document.getElementById("orderbtn").disabled = false;
                                    document.getElementById("wrongmsg").innerHTML = '';

                                    if (subtot == '' && discnt != '') {
                                        document.getElementById("discountcontent").innerHTML = '<div style="margin-left: 3%; margin-top: 3%;" class=" tst4 btn btn-danger">Please Enter quantity first</div>';
                                        document.getElementById("wrongmsg").innerHTML = '<strong><h5 style="  margin-left: 48%;margin-top: -3%;color: red;">Correct the Invalid Fields</h5></strong>';
                                    } else {
                                        document.getElementById("discountcontent").innerHTML = '';
                                        document.getElementById("wrongmsg").innerHTML = '';


                                    }
                                }
                            }
                        }
                    }
                }
            }    )
        }


    </script>
