<?php

/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 14/10/2018
 * Time: 18:00
 */
class OrderManage extends CI_Controller
{
    public $data=array('subview'=>'Oops subview not set','currentdata'=>'','itemList'=>'');



   //-------------------------------------------------------------------------------------------------------------------------------
    //Load Add user UI
    public function AddOrder()
    {
        $this->GetCurrentUserData();
        $this->load->model('Model_orders');
        //fetch items liat for dropdown
        $itemlist=$this->Model_orders->FetchItesForDropdown();
        $this->data['itemList']= $itemlist;

        $this->data['subview']='Orders/AddOrder';
        $this->load->view('Home',$this->data);
    }
    //Load View User UI
    public function ViewOrder()
    {
        $this->GetCurrentUserData();
        $this->data['subview']='Orders/ViewOrders';
        $this->load->view('Home',$this->data);
    }

    //Set user details in userprofile form update form
    public function  GetCurrentUserData()
    {
        $this->load->model('Model_user');
        $result=$this->Model_user->GetUserData();
        $this->data['currentdata']=$result;

    }

    //genarating orderid value
    public function GenarateOrderID()
    {
        $this->load->model('Model_orders');
        $result=$this->Model_orders->GetGenaratedOrderID();
        $output=array(
            'orderid' =>$result
        );

        echo json_encode($output);
    }

    //Filling item details fields according to choose item
    public function GetItemDataForFields()
    {
        $id = $this->input->get('chooseid');
        $this->load->model('Model_orders');
        $productunitprice=$this->Model_orders->DataRetrive('products','PID',$id,'unitprice');
        $productavailableamount=$this->Model_orders->DataRetrive('products','PID',$id,'availableamount');
        $output=array(
            'unitprice' => $productunitprice,
            'availableamount'=>$productavailableamount

        );

        echo json_encode($output);
    }

    //Insert order details
    public function InsertOrder()
    {
        $this->form_validation->set_rules('cusname', 'Customer Name', 'required');
        $this->form_validation->set_rules('cusaddress', 'Customer address', 'required');
        $this->form_validation->set_rules('quantity', 'Quantity', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->AddOrder();
        }else{
            $this->load->model('Model_orders');
            $result= $this->Model_orders->InsertNewOrder();
            if($result==1){
                $this->session->set_flashdata('msgS','Order Successfully Inserted !!');
                redirect('OrderManage/AddOrder');
            }else{
                $this->session->set_flashdata('msgW','Something went wrong !!');
                redirect('OrderManage/AddOrder');
            }
        }
    }

    //Load order data
    public function LoadOrderDataTable()
    {
        $query = '';
        $this->load->model('Model_orders');
        $this->load->library("pagination");
        $config = array();
        $config["base_url"] = "#";
        $config["total_rows"] = $this->Model_orders->count_all();
        $config["per_page"] = 5;
        $config["uri_segment"] = 3;
        $config["use_page_numbers"] = TRUE;
        $config["full_tag_open"] = '<ul class="pagination">';
        $config["full_tag_close"] = '</ul>';
        $config["first_tag_open"] = '<li>';
        $config["first_tag_close"] = '</li>';
        $config["last_tag_open"] = '<li>';
        $config["last_tag_close"] = '</li>';

        $config['prev_link'] = '<i class="fa fa-long-arrow-left"></i>Previous Page';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';


        $config['next_link'] = 'Next Page<i class="fa fa-long-arrow-right"></i>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';

        $config["cur_tag_open"] = "<li class='active'><a href='#'>";
        $config["cur_tag_close"] = "</a></li>";
        $config["num_tag_open"] = "<li>";
        $config["num_tag_close"] = "</li>";
        $config["num_links"] = 1;

        $this->pagination->initialize($config);
        $page = $this->uri->segment(3);
        $start = ($page - 1) * $config["per_page"];
        if ($this->input->get('querydata'))
        {
            $query = $this->input->get('querydata');
        }

        $output=array(
            'paginationdata' => $this->pagination->create_links(),
            'ordertable'  => $this->Model_orders->FetchingOrderDataForTable($config["per_page"],$start, $query)
        );

        echo json_encode($output);
    }

    //---------------------------------------------Report export---------------------------------------------------------------
    //Export Excel
    public function ExportAsExcel()
    {
        $this->load->model('Model_orders');
        $orderdata=$this->Model_orders->FetchforexcelOrders();
        $this->load->library("excel");
        $object=new PHPExcel();
        $object->setActiveSheetIndex(0);
        $table_columns=array("OrderID","Order Date","Item Name","Quantity","Delivary Date","Customer Name","Customer Address","Sub total","Tax amount","Net total","discount","Paid Amount","Due Amont","Payment Type","InsertBy");
        $column=0;
        foreach ($table_columns as $field)
        {
            $object->getActiveSheet()->setCellValueByColumnAndRow($column,1,$field);
            $column++;
        }

        $excel_row=2;
        foreach ($orderdata as $row)
        {
            $itemid=$this->Model_orders->DataRetrive('orderitems','orderID',$row->orderID,'itemID');
            $itemName=$this->Model_orders->DataRetrive('products','PID',$itemid,'productname');
            $Quantity=$this->Model_orders->DataRetrive('orderitems','orderID',$row->orderID,'quantity');
            $insertbyname=$this->Model_orders->DataRetrive('user','userid',$row->insertby,'username');

            $object->getActiveSheet()->setCellValueByColumnAndRow(0,$excel_row,$row->orderID);
            $object->getActiveSheet()->setCellValueByColumnAndRow(1,$excel_row,$row->orderdate);
            $object->getActiveSheet()->setCellValueByColumnAndRow(2,$excel_row,$itemName);
            $object->getActiveSheet()->setCellValueByColumnAndRow(3,$excel_row,$Quantity);
            $object->getActiveSheet()->setCellValueByColumnAndRow(4,$excel_row,$row->delivarydate);
            $object->getActiveSheet()->setCellValueByColumnAndRow(5,$excel_row,$row->customername);
            $object->getActiveSheet()->setCellValueByColumnAndRow(6,$excel_row,$row->customeraddress);
            $object->getActiveSheet()->setCellValueByColumnAndRow(7,$excel_row,$row->subtot);
            $object->getActiveSheet()->setCellValueByColumnAndRow(8,$excel_row,$row->tax);
            $object->getActiveSheet()->setCellValueByColumnAndRow(9,$excel_row,$row->nettot);
            $object->getActiveSheet()->setCellValueByColumnAndRow(10,$excel_row,$row->discount);
            $object->getActiveSheet()->setCellValueByColumnAndRow(11,$excel_row,$row->paidamount);
            $object->getActiveSheet()->setCellValueByColumnAndRow(12,$excel_row,$row->dueamount);
            $object->getActiveSheet()->setCellValueByColumnAndRow(13,$excel_row,$row->paymenttype);
            $object->getActiveSheet()->setCellValueByColumnAndRow(14,$excel_row,$insertbyname);


            $excel_row++;
        }

        $object_writer=PHPExcel_IOFactory::createWriter($object,'Excel5');
        header('Content-Type:application/vnd.ms-excel');
        header('Content-Disposition:attachment;filename="ProductOrder-Data.xls"');
        $object_writer->save('php://output');
    }

}