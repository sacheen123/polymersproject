<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

    //directing user to Login page
	public function index()
	{
            $this->load->view('Login');
	}

	//Login user methode
	public function LoginUser()
    {

        $this->form_validation->set_rules('uname', 'Username', 'required');
        $this->form_validation->set_rules('upassword', 'Password', 'required');
        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('Login');
        }
        else
        {
          $this->load->model('Model_user');
          $result= $this->Model_user->LoginUserData();
          if($result['id']==1){
              $this->session->set_flashdata('msg','This User have been deleted!!');
              redirect('Login/index');
          }elseif ($result['id']==2){
              $this->session->set_flashdata('msg','This User in Inactive State!!');
              redirect('Login/index');
          }elseif($result['id']==3){
              $this->session->set_flashdata('msg','User Crredentials Not Matched!!');
              redirect('Login/index');
          }else{

              $d=$result['dataset'];
              $type=$this->Model_user->DataRetrive('userroles','RoleID',$d[0]->type,'Role');
              $user_data=array(

                  'user_id'=>$d[0]->userid,
                  'username'=>$d[0]->username,
                  'type'=>$type,
                  'fname'=>$d[0]->fname,
                  'lname'=>$d[0]->lname,
                  'email'=>$d[0]->email,
                  'createDate'=>$d[0]->createDate,
                  'PhoneNumber'=>$d[0]->PhoneNumber,
                  'logged_in'=>TRUE
              );
             session_start() ;
             $this->session->set_userdata($user_data);
             redirect('MY_DashBoard/index');

          }
        }
    }

    //Logout function
    public  function  Logout(){
        $this->session->sess_destroy();
        redirect('Login/index');
    }
}

