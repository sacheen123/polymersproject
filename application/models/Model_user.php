<?php

class Model_user extends CI_Model
{

    //Getting logging user details
    function LoginUserData()
    {

        $deletedValue = '';
        $activeValue = '';
        $username = $this->input->post('uname', TRUE);
        $password = hash('sha512', $this->input->post('upassword', TRUE));

        $this->db->where('username', $username);
        $this->db->where('password', $password);

        $respond = $this->db->get('user');
        if ($respond->num_rows() == 1) {
            foreach ($respond->result() as $data) {
                $deletedValue = $data->isDeleted;
                $activeValue = $data->isActive;
            }
            if ($deletedValue == 1) {
                $dataset = array("id" => 1, "dataset" => '');
                return $dataset;
            } else {
                if ($activeValue == '1') {
                    $dataset = array("id" => 4, "dataset" => $respond->result());
                    return $dataset;
                } else {
                    $dataset = array("id" => 2, "dataset" => '');
                    return $dataset;
                }
            }

        } else {
            $dataset = array("id" => 3, "dataset" => '');
            return $dataset;
        }

    }


    //Get current logged in user data
    function GetUserData()
    {
        $this->db->where('userid', $this->session->userdata('user_id'));
        $respond = $this->db->get('user');

        return $respond->result();

    }

    //Getting perticular value from entered db table
    function DataRetrive($datatable, $feildvalue, $value, $returnfield)
    {

        $data = "";
        $sql = "SELECT * FROM " . $datatable . " WHERE " . $feildvalue . "=? AND isDeleted=0";
        $query = $this->db->query($sql, array($value));
        foreach ($query->result() as $row) {
            $data = $row->$returnfield;
        }

        return $data;
    }


    //Updating current user profile data
    function UpdateUserData()
    {
        $username = $this->input->post('uname', TRUE);
        $fname = $this->input->post('fname', TRUE);
        $lname = $this->input->post('lname', TRUE);
        $pnumber = $this->input->post('pnumber', TRUE);

        $sql = "UPDATE user SET           username=?,
                                          fname=?,
                                          lname=?,
                                          PhoneNumber=?
                                          WHERE userid=? ";
        $query = $this->db->query($sql, array($username, $fname, $lname, $pnumber, $this->session->userdata('user_id')));
        return $query;
    }


    //Updating user password
    function UpdatePassword()
    {
        $newpassword = hash('sha512', $this->input->post('npwd', TRUE));
        $enteredcurrentpassword = hash('sha512', $this->input->post('cpwd', TRUE));
        $data = $this->GetUserData();
        foreach ($data as $d) {
            $currentpassword = $d->password;
        }
        if ($currentpassword == $enteredcurrentpassword) {
            $sql = "UPDATE user SET  password=?  WHERE userid=? ";
            $this->db->query($sql, array($newpassword, $this->session->userdata('user_id')));
            return 'True';
        } else {
            return 'False';
        }
    }

    //Inserting New user
    function InsertNewUser()
    {
        $username = $this->input->post('uname', TRUE);
        $fname = $this->input->post('fname', TRUE);
        $lname = $this->input->post('lname', TRUE);
        $pnumber = $this->input->post('pnumber', TRUE);
        $password = $this->input->post('password', TRUE);
        $cpassword = $this->input->post('cpassword', TRUE);
        $email = $this->input->post('email', TRUE);
        $role = $this->input->post('role', TRUE);
        $date = date('Y-m-d H:i:s');

        $UserAvailability = $this->DataRetrive('user', 'username', $username, 'username');
        if ($UserAvailability == '') {
            if ($password != $cpassword) {
                return 2; //passwords not matched
            } else {
                $hashedpassword = hash('sha512', $password);
                $sql = "INSERT INTO user (username,password,type,fname,lname,email,PhoneNumber,CreateDate,isActive,isDeleted) values (?,?,?,?,?,?,?,?,'1','0') ";
                $this->db->query($sql, array($username, $hashedpassword, $role, $fname, $lname, $email, $pnumber, $date));
                return 3; //successfully inserted
            }
        } else {
            return 1; //username already available
        }
    }

    function UpdateCurrentUser($uid)
    {
        $fname = $this->input->post('fname', TRUE);
        $lname = $this->input->post('lname', TRUE);
        $pnumber = $this->input->post('pnumber', TRUE);
        $email = $this->input->post('email', TRUE);
        $role = $this->input->post('role', TRUE);

        $sql = "UPDATE user SET type=?,fname=?,lname=?,email=?,PhoneNumber=? WHERE userid=? ";
        $result = $this->db->query($sql, array($role, $fname, $lname, $email, $pnumber, $uid));
        return $result;
    }


    //count all users for pagination function
    function count_all()
    {
        $this->db->select("*");
        $this->db->from("user");
        $this->db->where("isDeleted", "0");
        $query = $this->db->get();
        return $query->num_rows();
    }

    //fetching userdata required for table
    function FetchingUserDataForTable($limit, $start, $searchquery)
    {
        $output = '';
        $this->db->select("*");
        $this->db->from("user");
        $this->db->where("isDeleted", "0");
        if ($searchquery != '') {
            $this->db->like('username', $searchquery);
            $this->db->or_like('fname', $searchquery);
            $this->db->or_like('lname', $searchquery);
            $this->db->or_like('email', $searchquery);
        }
        $this->db->order_by("userid");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if ($query->num_rows() != 0) {
            $output .= '
        <table class="display nowrap table table-hover table-striped table-bordered dataTable"
           cellspacing="0" width="100%" role="grid" aria-describedby="example23_info" style="width: 100%;">
        <thead>
        <tr role="row">
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending"
                aria-label="Name: activate to sort column descending" style="width: 175px;">UserName
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Position: activate to sort column ascending" style="width: 254px;">Fname
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Office: activate to sort column ascending" style="width: 133px;">Lname
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Age: activate to sort column ascending" style="width: 67px;">Type
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Start date: activate to sort column ascending" style="width: 127px;">Email
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Start date: activate to sort column ascending" style="width: 127px;">C.Status
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Salary: activate to sort column ascending" style="width: 110px;">Action
            </th>
        </tr>
        </thead>
            ';
            foreach ($query->result() as $row) {
                $activestate = '';
                $roleType = '';

                if ($this->DataRetrive('userroles', 'RoleID', $row->type, 'Role') == 'Admin') {
                    $roleType = '<span class="label label-danger">Admin</span>';
                } elseif ($this->DataRetrive('userroles', 'RoleID', $row->type, 'Role') == 'StockManager') {
                    $roleType = '<span class="label label-info">StockManager</span>';
                }

                if ($row->isActive == 1) {
                    $activestate = '<a  onclick="GetactiveUserID(\'' . $row->userid . '\')" name="sac" style="padding: 6px;"   role="button" class="btn btn-outline-success">Active</a>';
                } else {
                    $activestate = '<a onclick="GetInactiveUserID(\'' . $row->userid . '\')" name="sac" style="padding: 6px;" role="button" value="' . $row->fname . '" class="btn btn-outline-danger">InActive</a>';
                }
                $output .= '
           <tr>
             <td>' . $row->username . '</td>
             <td>' . $row->fname . '</td>
             <td>' . $row->lname . '</td>
             <td>' . $roleType . '</td>
             <td>' . $row->email . '</td>    
             <td>' . $activestate . '</td>
             <td class="text-nowrap">
                <div style=" margin-left: 20%;">
                    <a  href=\'' . base_url() . "index.php/UsersManage/EditUser?id=$row->userid" . '\'  data-toggle="tooltip" data-original-title="Edit"> <i  class="fa fa-pencil  m-r-10"></i> </a>
                    <a style="cursor: pointer;" onclick="DeleteUser(\'' . $row->userid . '\')"  data-toggle="tooltip" data-original-title="Delete"> <i class="fa fa-close text-danger"></i> </a>                 
                 </div>
                           
             </td>
           </tr>
           ';
            }
            $output .= '</table>';
        } else {
            $output .= '<tr>
       <td colspan="5">No Data Found</td>
      </tr>
      
      ';
        }
        return $output;
    }


    //Making Inactive User
    public function SetInactiveUser($id)
    {
        $sql = "UPDATE user SET  isActive='0'  WHERE userid=? ";
        $this->db->query($sql, array($id));
        return 'true';
    }

    //Making active user
    public function SetactiveUser($id)
    {
        $sql = "UPDATE user SET  isActive='1'  WHERE userid=? ";
        $this->db->query($sql, array($id));
        return 'true';
    }

    //delete user
    public function DeleteUser($id)
    {
        $sql = "UPDATE user SET  isDeleted='1'  WHERE userid=? ";
        $this->db->query($sql, array($id));
        return 'true';
    }

    //Fetching requested user details to load in update user form
    public function GetRequestedUserData($userid)
    {
        $sql = "SELECT * FROM user WHERE userid=?";
        $result = $this->db->query($sql, array($userid));
        return $result->result();
    }

    //Fetch user data for export in excel sheet
    public function Fetchforexcel()
    {
        $sql = "SELECT * FROM user WHERE isDeleted='0'";
        $result = $this->db->query($sql);
        return $result->result();

    }

    public function GetNotifications()
    {
        $this->db->select("*");
        $this->db->from(" notification");
        $this->db->order_by("NID");
        $this->db->limit(1, 20);
        $query = $this->db->get();

        return $query->result();
    }
}