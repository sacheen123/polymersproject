<?php

/**
 * Created by PhpStorm.
 * User: sacheen
 * Date: 20/11/2018
 * Time: 10:41
 */
class Model_orders extends CI_Model
{

    //Getting perticular value from entered db table
    function DataRetrive($datatable, $feildvalue, $value, $returnfield)
    {

        $data = "";
        $sql = "SELECT * FROM " . $datatable . " WHERE " . $feildvalue . "=? AND isDeleted=0";
        $query = $this->db->query($sql, array($value));
        foreach ($query->result() as $row) {
            $data = $row->$returnfield;
        }

        return $data;
    }

    //--------------------------------------------------------------------------------------------------------------------------------
    //genarating order id
    public function GetGenaratedOrderID()
    {
        $this->db->select("*");
        $this->db->from("orders");
        $query = $this->db->get();
        $numrows=$query->num_rows();
        $year = date('Y');
        $month = date('m');
        $day=date('d');
        $oredrID="ORD-".$year.$month.$day.$numrows;
        return $oredrID;
    }

    //fetch itemlist for display in dropdowns
    public function FetchItesForDropdown()
    {
        $this->db->select("*");
        $this->db->from("products");
        $this->db->where("isDeleted", "0");
        $this->db->order_by("PID");
        $query = $this->db->get();
        return $query->result();
    }

    //insert new orders
    public function InsertNewOrder()
    {
        $orderid = $this->input->post('orderid', TRUE);
        $orderdate = $this->input->post('orderdate', TRUE);
        $delivarydate = $this->input->post('delivarydate', TRUE);
        $customername = $this->input->post('cusname', TRUE);
        $customeraddress = $this->input->post('cusaddress', TRUE);
        $itemid=$this->input->post('itemname', TRUE);
        $availableamont=$this->input->post('availableamount', TRUE);
        $quantity=$this->input->post('quantity', TRUE);
        $subtotal=$this->input->post('subtot', TRUE);
        $tax=$this->input->post('tax', TRUE);
        $nettotal=$this->input->post('nettotal', TRUE);
        $discount=$this->input->post('discount', TRUE);
        $paymenttype=$this->input->post('paymenttype', TRUE);
        $paidamount=$this->input->post('paidamount', TRUE);
        $dueamount=$this->input->post('dueamount', TRUE);
        $criticalamount=$this->DataRetrive('products','PID',$itemid,'criticalamount');
        $itemname=$this->DataRetrive('products','PID',$itemid,'productname');
        $date = date('Y-m-d H:i:s');
        $currentuser= $this->session->userdata('user_id');
        $itemtotalprice=$this->input->post('itemtotalprice', TRUE);


        if($availableamont-$quantity==$criticalamount || $availableamont-$quantity<$criticalamount) //checking stock equal to critical amount or less
        {
            //stock over give notification
            $NotificationHeader = "Product Out Stock";
            $NotificationBody = $itemname . " Out Of stock";

            $sql = "INSERT INTO Notification (NotificationHeader,NotificationBody,notificationtype,NotificationDate,NotificationStatus) values (?,?,'product',?,0) ";
            $resultNot = $this->db->query($sql, array($NotificationHeader, $NotificationBody, $date));
            if ($resultNot != 1) {
                return 2;//notification enter issue
            } else {
                $AudioLink = base_url() . "assets/music/tone.MP3";
                $audio = "<embed src='" . $AudioLink . "'>";
                echo $audio;


                //checking if stock is over and send request to production
                if ($availableamont - $quantity < 0) {
                    $ProductionRequestAmount = $quantity - $availableamont; //production requesting amount
                    $NotificationHeader = "New Production Request";
                    $NotificationBody = $ProductionRequestAmount." ".$itemname . " Required";

                    $ProductionRequestSql = "INSERT INTO productionrequest (productid,oderID,requestquantity,duedate,insertdate,state,isDeleted) values (?,?,?,?,?,1,0)";
                    $ProductStockUpdateSql = "UPDATE products SET  availableamount='0'  WHERE PID=? ";
                    $sqlproductionrequestnotification = "INSERT INTO Notification (NotificationHeader,NotificationBody,notificationtype,NotificationDate,NotificationStatus) values (?,?,'productionrequest',?,0) ";


                    $resultProductrequest = $this->db->query($ProductionRequestSql, array($itemid,$orderid, $ProductionRequestAmount, $delivarydate, $date));
                    $resultProductStockupdate = $this->db->query($ProductStockUpdateSql, array($itemid));
                    $this->db->query($sqlproductionrequestnotification, array($NotificationHeader, $NotificationBody, $date));
                    if ($resultProductrequest != 1 || $resultProductStockupdate != 1) {
                        return 3;//production request or stock amount update failed
                    }

                } else {
                    $stockreduce = $availableamont - $quantity;   //stock reducing amount
                    $ProductStockUpdateSql = "UPDATE products SET  availableamount=?  WHERE PID=? ";
                    $resultProductStockupdate = $this->db->query($ProductStockUpdateSql, array($stockreduce, $itemid));
                    if ($resultProductStockupdate != 1) {
                        return 4;// stock amount update failed
                    }
                }

                //Adding order details to order table and product item table
                $ordersql = "INSERT INTO orders (orderID,orderdate,delivarydate,customername,customeraddress,subtot,tax,discount,nettot,paymenttype,paidamount,dueamount,isDeleted,	insertdate,insertby) values (?,?,?,?,?,?,?,?,?,?,?,?,'0',?,?) ";
                $producitemsql="INSERT INTO orderitems (orderID,itemID,	quantity,totprice) values (?,?,?,?)";
                $resultorder = $this->db->query($ordersql, array($orderid, $orderdate, $delivarydate, $customername, $customeraddress, $subtotal, $tax, $discount, $nettotal, $paymenttype, $paidamount, $dueamount, $date, $currentuser));
                $resultProduct = $this->db->query($producitemsql, array($orderid,$itemid,$quantity,$itemtotalprice));
                if ($resultorder == 1 && $resultProduct==1) {
                    return 1;
                } else {
                    return 5; //order enter issue in critical amount of product
                }
            }
        }else{
            $normalordersql = "INSERT INTO orders (orderID,orderdate,delivarydate,customername,customeraddress,subtot,	tax,discount,nettot,paymenttype,paidamount,dueamount,isDeleted,	insertdate,insertby) values (?,?,?,?,?,?,?,?,?,?,?,?,'0',?,?) ";
            $producitemsql="INSERT INTO orderitems (orderID,itemID,	quantity,totprice) values (?,?,?,?)";
            $result = $this->db->query($normalordersql, array($orderid,$orderdate,$delivarydate,$customername, $customeraddress,$subtotal,$tax,$discount,$nettotal,$paymenttype,$paidamount,$dueamount,$date,$currentuser));
            $resultProduct = $this->db->query($producitemsql, array($orderid,$itemid,$quantity,$itemtotalprice));


            if ($result == 1 && $resultProduct==1) {
                return 1;
            } else {
                return 6; //order insetr issue
            }
        }

    }

    //count all orders require for order table
    public function count_all()
    {
        $this->db->select("*");
        $this->db->from("orders");
        $this->db->where("isDeleted", "0");
        $query = $this->db->get();
        return $query->num_rows();
    }

    //fetching data for order table
    public function FetchingOrderDataForTable($limit, $start, $searchquery)
    {
        $output = '';
        $this->db->select("*");
        $this->db->from("orders");
        $this->db->where("isDeleted","0");
        if ($searchquery!=''){
            $this->db->like('customername', $searchquery);
            $this->db->or_like('orderdate', $searchquery);
            $this->db->or_like('orderID', $searchquery);
            $this->db->or_like('delivarydate', $searchquery);
        }
        $this->db->order_by("orderID");
        $this->db->limit($limit, $start);
        $query = $this->db->get();
        if($query->num_rows()!=0) {
            $output .= '
        <table class="display nowrap table table-hover table-striped table-bordered dataTable"
           cellspacing="0" width="100%" role="grid" aria-describedby="example23_info" style="width: 100%;">
        <thead>
        <tr role="row">
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1" aria-sort="ascending"
                aria-label="Name: activate to sort column descending" style="width: 175px;">OrderID
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Position: activate to sort column ascending" style="width: 254px;">Order Date
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Office: activate to sort column ascending" style="width: 133px;">Delivary Date
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Age: activate to sort column ascending" style="width: 67px;">Customer Name
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Start date: activate to sort column ascending" style="width: 127px;">Customer Address
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Start date: activate to sort column ascending" style="width: 127px;">Net Total
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Salary: activate to sort column ascending" style="width: 110px;">Paid Amount 
            </th>
             <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Salary: activate to sort column ascending" style="width: 110px;">Due Amount 
            </th>
             <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Salary: activate to sort column ascending" style="width: 110px;">Payment Type 
            </th>
            <th class="sorting" tabindex="0" aria-controls="example23" rowspan="1" colspan="1"
                aria-label="Salary: activate to sort column ascending" style="width: 110px;">Insert By
            </th>

        </tr>
        </thead>
            ';
            foreach ($query->result() as $row) {
                $inserUsername=$this->DataRetrive("user","userid",$row->insertby,"username");
                $payType='';

                if($row->paymenttype=="Cash"){
                    $payType='<span class="label label-danger">Cash</span>';
                }else{
                    $payType='<span class="label label-info">Cheque</span>';
                }

                $output .= '
           <tr>
             <td>' . $row->	orderID . '</td>
             <td>' . $row->	orderdate . '</td>
             <td>' . $row->delivarydate . '</td>
             <td>' . $row->	customername . '</td>
             <td>' . $row->customeraddress . '</td>
             <td>' . $row->	nettot . '</td>
             <td>' . $row->paidamount . '</td>
             <td>' . $row->	dueamount . '</td>
             <td>' . $payType . '</td>
             <td>' .$inserUsername.'</td>

           </tr>
           ';
            }
            $output .= '</table>';
        }else{
            $output .= '<tr>
       <td colspan="5">No Data Found</td>
      </tr>
      
      ';
        }
        return $output;
    }

    //fetching data for exel
    public function FetchforexcelOrders()
    {
        $sql = "SELECT * FROM orders WHERE isDeleted='0'";
        $result = $this->db->query($sql);
        return $result->result();
    }
}